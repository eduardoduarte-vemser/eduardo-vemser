import React, { Component } from 'react';
import '../App.css';

import ListaSeries from '../models/listaSeries'
import Input from '../componentes/Input'
import { Link } from 'react-router-dom';

class Jsflix extends Component {
    

  constructor( props ){
    super( props )
    this.listaseries = new ListaSeries();
    console.log( this.listaseries.invalidas() );
    
    this.state = {
      funcaoInvalida: null,
      funcaoFiltrar: null,
      funcaoPorNome: null,
      valorInput: null,
      mediaDeEpisodios: null,
      funcaoSalario: null,
      funcaoGenero: null,
      funcaoTitulo: null,
      funcaoCredito: null
    }
  }

  credito(){
    const {valorInput} = this.state
        let filtrar = this.listaseries.creditos( valorInput );
        // filtrar = filtrar.map(filtro => filtro.titulo)
        this.setState({
          funcaoCredito: filtrar.toString()
        })
        setTimeout(() => {
          this.setState({
            funcaoCredito: null
          })
        }, 5000);
      }

      titulo(){
        const {valorInput} = this.state
        let filtrar = this.listaseries.queroTitulo( valorInput );
        // filtrar = filtrar.map(filtro => filtro.titulo)
        this.setState({
          funcaoTitulo: filtrar.toString()
        })
        setTimeout(() => {
          this.setState({
            funcaoTitulo: null
          })
        }, 5000);
      }

      invalidas(){
        const invalida = this.listaseries.invalidas();
        this.setState({
          funcaoInvalida: invalida
        })
      }

      atualizacaoInput(evt) {
        this.setState({
          valorInput: evt.target.value
        })
      }

      filtrarAno(){
        const {valorInput} = this.state
        let filtrar = this.listaseries.filtrarPorAno( valorInput );
        filtrar = filtrar.map(filtro => filtro.titulo)
        this.setState({
          funcaoFiltrar: filtrar.toString()
        })
        setTimeout(() => {
          this.setState({
            funcaoFiltrar: null
          })
        }, 5000);
      }
      
      filtrarNome(){
        const {valorInput} = this.state
        let filtrar = this.listaseries.procuraPorNome( valorInput );
        // filtrar = filtrar.map(filtro => filtro.titulo)
        this.setState({
          funcaoPorNome: filtrar.toString()
        })
        setTimeout(() => {
          this.setState({
            funcaoPorNome: null
          })
        }, 5000);
      }

      salario(){
        const {valorInput} = this.state
        let msg = `Informar um indice valido entre 0 e 9`
        if(valorInput < 0 || valorInput == null){
          this.setState({
            funcaoSalario: msg
          })
        }else{
          let filtrar = this.listaseries.totalSalarios( valorInput );
          // filtrar = filtrar.map(filtro => filtro.titulo)
          this.setState({
            funcaoSalario: filtrar.toString()
          })
          setTimeout(() => {
            this.setState({
              funcaoSalario: null
            })
          }, 5000);
        }
      }

      genero(){
        const {valorInput} = this.state
        let filtrar = this.listaseries.queroGenero( valorInput );
        this.setState({
          funcaoGenero: filtrar.toString()
        })
        setTimeout(() => {
          this.setState({
            funcaoGenero: null
          })
        }, 5000);

      }

      mediaEpisodios(){
        const media = this.listaseries.mediaDeEpisodios();
        this.setState({
          mediaDeEpisodios: media
        })
      }

    render() {
        const { funcaoInvalida, funcaoFiltrar, funcaoPorNome,  mediaDeEpisodios, funcaoSalario, funcaoGenero, funcaoTitulo, funcaoCredito } = this.state
        return (
        <div className="App">
            <button onClick={this.invalidas.bind( this )}>Função Invalidas </button><br></br>
            <p>{funcaoInvalida}</p>
            <Input funcao = {this.filtrarAno.bind( this )} valor={this.atualizacaoInput.bind(this )} nome="Funcao Por Ano"/>
            <p>{funcaoFiltrar}</p>
            <Input funcao = {this.filtrarNome.bind( this )} valor={this.atualizacaoInput.bind(this )} nome="Funcao Por Nome"/>
            <p>{funcaoPorNome}</p>
            <button onClick={this.mediaEpisodios.bind( this )}>Função Media de episodios </button><br></br>
            <p>{mediaDeEpisodios}</p>
            <Input funcao = {this.salario.bind( this )} valor={this.atualizacaoInput.bind(this )} nome="Funcao Buscar salario"/>
            <p>{funcaoSalario}</p>
            <Input funcao = {this.genero.bind( this )} valor={this.atualizacaoInput.bind(this )} nome="Funcao Buscar Genero"/>
            <p>{funcaoGenero}</p>
            <Input funcao = {this.titulo.bind( this )} valor={this.atualizacaoInput.bind(this )} nome="Funcao Buscar Titulo"/>
            <p>{funcaoTitulo}</p>
            <Input funcao = {this.credito.bind( this )} valor={this.atualizacaoInput.bind(this )} nome="Funcao Buscar Credito"/>
            <p>{funcaoCredito}</p>
            <Link to="/react_mirror">Ir pra React_Mirror</Link><br></br>
            <Link to="/">Voltar para pagina Inicial</Link>
        </div>
        );
    }
    
}



export default Jsflix;
