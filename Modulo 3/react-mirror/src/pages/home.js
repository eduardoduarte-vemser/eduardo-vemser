import React, { Component } from 'react';
import '../App.css';
import { Link } from 'react-router-dom';

class Home extends Component {
    
    render() {

        return (
        <div className="App">
            <h1>Pagina Inicial do projeto</h1>
            <Link to="/series">Ir para Jsflix</Link><br></br>
            <Link to="/react_mirror">Ir para React_Mirror</Link>
        </div>
        );
    }
}



export default Home;