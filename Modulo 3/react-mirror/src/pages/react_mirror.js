import React, { Component } from 'react';
import '../App.css';
import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from '../componentes/episodioUi';
import { Link } from 'react-router-dom';
import MensagemFlash from '../componentes/MensagemFlash'
import MeuInputNumero from '../componentes/MeuInputNumero';

class React_mirror extends Component {
  
  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    this.sortear = this.sortear.bind( this );
    this.assistido = this.assistido.bind( this );
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      nome: "teste",
      nota: null,
      deveExibirMensagem: false,
      msgValidacao: null,
      tempoAlerta: null,
      devoExibirErro: false
    }
  }

  sortear(){
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  atualizarMensagem = devoExibir => {
    console.log(devoExibir);
    
    this.setState({
      deveExibirMensagem: devoExibir
    })
  }

  fecharAutomaticoMsg = tempo => {
      setTimeout(() => {
        this.setState({
          deveExibirMensagem: false        } )
      }, 1000);
      
  }

  assistido() {
    const { episodio } = this.state
    episodio.marcarComoAssistido()
    this.setState( {
      assistido: true
    } )
  }

  registrar( { nota, erro } ) {
    this.setState ({
      devoExibirErro: erro
    })
    if( erro ) {
      return
    }
    const { episodio } = this.state
    if(episodio.avaliar( nota )) {
      this.setState( {
        episodio,
        deveExibirMensagem: true,
        msgValidacao: null
      } )
      }else{
        this.setState( {
        episodio,
        msgValidacao: "Informar uma nota válida (entre 1 e 5)"
      } )
    }
  }

  // geraCampoDeNota() {
  //   return (

  //     <div>
  //     {
  //       this.state.episodio.assistido && (
  //         <div>
  //           <span> Nota do Episodio</span>
  //           <input type="number" placeholder="Nota de 1 a 5" min="1" max="5" onBlur = { this.registrar.bind(this) }></input>
  //         </div>
  //       )
  //     }
  //     </div>
      
  //   )
  // }

  geraNota(){
    
    return(
      <div>
      {
      this.state.episodio.nota &&(
        <span>{ this.state.episodio.mostraNota }</span>
      )
      }
      </div>)
  }


  render() {

    const { episodio, deveExibirMensagem, nota, msgValidacao, devoExibirErro} = this.state
    const {listaEpisodios} = this
    return (
      <div className="App">
          <MensagemFlash fecharAutomaticoMsg = {this.fecharAutomaticoMsg}
            atualizarMensagem={ this.atualizarMensagem }
                          deveExibirMensagem={ deveExibirMensagem} 
                          mensagem="Registramos sua nota!" />
        <header className="App-header">
        <EpisodioUi episodio={ episodio } />
        
          <div className="botoes">
            <button onClick={ this.sortear }>Proximo</button>
            <button onClick={ this.assistido }>Já assisti</button>
            <button className="btn btn-vermelho">
              <Link to= {{ pathname: "/avaliacoes", state: {listaEpisodios}}} className="link-avaliacoes"/* "/avaliacoes" */>Avaliações </Link>
            </button>
          </div>
          {/* { this.geraCampoDeNota() } */}
          <MeuInputNumero placeholder="1 a 5" 
                          mensagem campo = "Qual sua nota para o episodio"
                          visivel= {episodio.assistido || false}
                          obrigatorio ={ true }
                          atualizarValor = { this.registrar.bind( this ) }
                          devoExibirErro = { devoExibirErro }/>
          {nota == null ? ( <span className="validacao" >{msgValidacao}</span> ) : this.geraNota()}
          <Link to="/series">Ir pra Jsflix</Link><br></br>
          <Link to="/">Voltar para pagina Inicial</Link>
          
        </header>
        
      </div>
    );
  }
}

export default React_mirror;
