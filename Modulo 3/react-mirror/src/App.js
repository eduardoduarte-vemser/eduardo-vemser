import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import React_mirror from './pages/react_mirror';
import Jsflix from './pages/jsflix';
import Home from './pages/home';
import ListaDeAvaliacoes from './componentes/ListaDeAvaliacoes'
import TelaDetalheEpisodio from './componentes/TelaDetalheEpisodio'

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home }/>
        <Route path="/series" component={ Jsflix } />
        <Route path="/react_mirror" component={ React_mirror } />
        <Route path="/avaliacoes" component={ListaDeAvaliacoes} />
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio } />
      </Router>
    )
  }
}
