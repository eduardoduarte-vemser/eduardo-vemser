export default class Episodio {
    constructor( id, nome, duracao, temporada, ordemEpisodio, thumbUrl ) {
        this.id = id
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordem = ordemEpisodio
        this.url = thumbUrl
        this.qtdVezesAssistido = 0
        this.msgValidacao = null
    }

    marcarComoAssistido(){
        this.assistido = true
        this.qtdVezesAssistido += 1 
    }

    validarNota( nota ){
        if( nota < 1 || nota > 5){
            return false
        }
        return true
    }

    avaliar( nota ){
        if( this.validarNota( nota ) ){
            this.nota = parseInt( nota )
            return true
        } else {
            return false
        }
    }


    get duracaoEmMin(){
        return `${ this.duracao } min`
    }

    get temporadaEpisodio() {
        return `${ this.temporada.toString().padStart( 2, '0') }/${ this.ordem.toString().padStart( 2, '0') }`
    }

    get mostraNota(){
        return `Nota: ${this.nota}`
    }

}