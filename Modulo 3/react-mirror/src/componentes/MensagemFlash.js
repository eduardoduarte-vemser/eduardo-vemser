import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {

    constructor( props ) {
        super( props )
        this.idsTimeouts = []
        this.animacao = ''

    }

    fechar = () => {
        this.props.atualizarMensagem( false )
    }

    limparTimeouts(){
        this.idsTimeouts.forEach( clearTimeout )
    }

    componentWillUnmount() {
        this.limparTimeouts()
    }

    componentDidUpdate( prevProps ){
        const { deveExibirMensagem, alerta } = this.props
        if( prevProps.deveExibirMensagem !== deveExibirMensagem ){
            const novoIdTimeout = setTimeout(() => {
                this.fechar()
            }, alerta)
            this.idsTimeouts.push( novoIdTimeout )
        }   
    }

    render(){
        const { deveExibirMensagem, mensagem, cor} = this.props

        console.log(this.animacao, deveExibirMensagem)
        if(this.animacao || deveExibirMensagem) {
            this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
        }
        return (
            
           <span onClick={ this.fechar}  className={ `flash ${ cor } ${this.animacao}` }>{ mensagem }</span>
           
        )
    }
}

// https://reactjs.org/docs/typechecking-with-proptypes.html
MensagemFlash.propTypes = {
    mensagem: PropTypes.string.isRequired,
    deveExibirMensagem: PropTypes.bool.isRequired,
    atualizarMensagem: PropTypes.func.isRequired,
    cor: PropTypes.oneOf(['verde','vermelho']),
    alerta: PropTypes.number.isRequired
}

MensagemFlash.defaultProps = {
    cor: 'verde',
    alerta: 3000
}