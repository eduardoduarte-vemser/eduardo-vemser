import React from 'react';
import { Link } from 'react-router-dom';

const ListaDeAvaliacoes = props => {
    const { listaEpisodios } = props.location.state
    return listaEpisodios.avaliados.map(ep => 
             <li key={ ep.id }>
                <Link to={{ pathname: `/episodio/${ ep.id }`, state: { episodio: ep }}}>
                    {`${ep.nome} - ${ep.nota} Temporada: ${ep.temporada} Episodio: ${ep.ordem}`}
                </Link>
             </li>
    )
  }
  
  export default ListaDeAvaliacoes