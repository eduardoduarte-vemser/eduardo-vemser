import React, { Component } from 'react';

export default class EpisodioUi extends Component {

    render() {
        const { episodio } = this.props
        return (
            <React.Fragment>
                <h2>{ episodio.nome }</h2>
                <img src={ episodio.url } alt={ episodio.nome } title={ episodio.nome }></img>
                <span className="nota"></span>
                <span>{ episodio.assistido ? 'Sim' : 'Não'}, { episodio.qtdVezesAssistido } vezes </span>
                <h4>Temporada / Episodio { episodio.temporadaEpisodio }</h4>
                <h5>Duração: { episodio.duracaoEmMin }</h5>
            </React.Fragment>
        )
    }

}