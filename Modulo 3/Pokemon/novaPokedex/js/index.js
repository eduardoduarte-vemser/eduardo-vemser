
const numeros = document.getElementsByClassName( 'numeros' );
const busca = document.getElementById( 'inputDisplay' );
const botaoBuscar = document.getElementById( 'enviar' );
const sorteado = [];
const proximo = document.getElementById( 'proximo' );
const anterior = document.getElementById( 'anterior' );
const sortear = document.getElementById( 'sorteador' );
let numeroPokemon = '';


for ( let i = 0; i < numeros.length; i += 1 ) {
  numeros[i].addEventListener( 'click', ( event ) => {
    if ( event.screenX !== 0 ) {
      busca.value += i;
    }
  } );
}

document.addEventListener( 'keyup', ( event ) => {
  if ( event.keyCode === 13 ) {
    if ( busca.value !== '' ) {
      event.preventDefault();
      document.getElementById( 'enviar' ).click();
    }
  }

  if ( event.keyCode === 13 ) {
    event.preventDefault();
    document.getElementById( 'enviar' ).click();
  }
} );

const pokeApi = new PokeApi(); // eslint-disable-line no-undef


function renderizacaoPokemon( pokemon ) {
  const imagem = document.getElementById( 'imagem' );
  const nome = document.getElementById( 'nome' );
  const altura = document.getElementById( 'altura' );
  const idPokemon = document.getElementById( 'id' );
  const peso = document.getElementById( 'peso' );
  imagem.src = pokemon.imagem;
  nome.innerHTML = `Nome: ${ pokemon.nome }`;
  altura.innerHTML = `Altura: ${ pokemon.altura } cm`;
  peso.innerHTML = `Peso ${ pokemon.peso } Kg`;
  idPokemon.innerHTML = `ID: ${ pokemon.id }`;
  pokemon.listarEstatisticas();
  pokemon.listarTipos();
  pokemon.fundo();
}

async function buscar( id ) {
  const pokemonEspecifico = await pokeApi.buscar( id );
  const poke = new Pokemon( pokemonEspecifico ); // eslint-disable-line no-undef
  renderizacaoPokemon( poke );
}

busca.addEventListener( 'blur', () => {
  numeroPokemon = busca.value;
  if ( sorteado[sorteado.length - 1] !== numeroPokemon ) {
    buscar( numeroPokemon );
    sorteado.push( numeroPokemon );
  }
} );

botaoBuscar.addEventListener( 'click', () => {
  numeroPokemon = busca.value;
  if ( sorteado[sorteado.length - 1] !== numeroPokemon ) {
    buscar( numeroPokemon );
    sorteado.push( numeroPokemon );
  }
} );

// avancar para o proximo Pokemon

proximo.addEventListener( 'click', ( event ) => {
  if ( event.screenX !== 0 ) {
    numeroPokemon = parseInt( numeroPokemon, 10 ) + 1;
    buscar( numeroPokemon );
  }
} );

// retornar para o pokemon anterior

anterior.addEventListener( 'click', ( event ) => {
  if ( event.screenX !== 0 ) {
    numeroPokemon = parseInt( numeroPokemon, 10 ) - 1;
    this.buscar( numeroPokemon );
  }
} );

sortear.addEventListener( 'click', ( event ) => {
  let i = 1;
  while ( i > 0 ) {
    numeroPokemon = Math.floor( Math.random() * 802 + 1 );
    i = 0;
    for ( let j = 0; j < sorteado.length; j += 1 ) {
      if ( sorteado[j] === numeroPokemon ) {
        i += 1;
      }
    }
  }
  if ( event.screenX !== 0 ) {
    this.buscar( numeroPokemon );
    sorteado.push( numeroPokemon );
  }
} );
