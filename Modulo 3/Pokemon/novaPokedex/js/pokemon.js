class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name;
    this.id = obj.id;
    this.altura = ( obj.height * 10 );
    this.peso = ( obj.weight / 10 );
    this.estatisticas = obj.stats;
    this.tipos = obj.types;
    this.imagem = obj.sprites.front_default;
  }

  listarEstatisticas() {
    const pai = document.getElementById( 'estatisticas' );
    const filho = pai.querySelector( 'ul' );
    const filhoSpan = pai.querySelector( 'span' );

    if ( filho !== null ) {
      filho.parentNode.removeChild( filho );
      filhoSpan.parentNode.removeChild( filhoSpan );
    }
    const div = document.getElementById( 'estatisticas' );
    const span = document.createElement( 'span' );
    span.innerHTML = 'Estatisticas';
    div.appendChild( span );
    const ul = document.createElement( 'ul' );
    div.appendChild( ul );
    for ( let i = 1; i < this.estatisticas.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.innerHTML = `${ this.estatisticas[i].stat.name } ${ this.estatisticas[i].base_stat }`;
      ul.appendChild( li );
    }
  }

  listarTipos() {
    const pai = document.getElementById( 'tipos' );
    const filho = pai.querySelector( 'ul' );
    const filhoSpan = pai.querySelector( 'span' );

    if ( filho !== null ) {
      filho.parentNode.removeChild( filho );
      filhoSpan.parentNode.removeChild( filhoSpan );
    }
    const div = document.getElementById( 'tipos' );
    const span = document.createElement( 'span' );
    span.innerHTML = 'Tipos';
    div.appendChild( span );
    const ul = document.createElement( 'ul' );
    div.appendChild( ul );
    for ( let i = 0; i < this.tipos.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.innerHTML = this.tipos[i].type.name;
      ul.appendChild( li );
    }
  }

  fundo() {
    const fundoImagem = document.querySelector( '.img-pokemon' );
    switch ( this.tipos[0].type.name ) {
      case 'water':
        fundoImagem.id = 'fundo-agua';
        break;
      case 'fire':
        fundoImagem.id = 'fundo-fogo';
        break;
      case 'ice':
        fundoImagem.id = 'fundo-gelo';
        break;
      case 'electric':
        fundoImagem.id = 'fundo-relampago';
        break;
      default:
        fundoImagem.id = 'fundo-paisagem';
        break;
    }
  }
}
