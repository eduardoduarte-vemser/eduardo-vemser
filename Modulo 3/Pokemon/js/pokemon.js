
const api = 'https://pokeapi.co/api/v2/pokemon/';

const buscar = document.getElementById('inputDisplay'),
      botaoBuscar = document.getElementById('enviar')
;
let numeroPokemon, pokemon;

let numeros = document.getElementsByClassName('numeros');
let proximo = document.getElementById('proximo');
let anterior = document.getElementById('anterior');
let sortear = document.getElementById('sorteador');
let fundoImagem = document.querySelector('.img-pokemon');
let sorteado = [];
let msg = "";
// habilitar botoes numericos da pokedex
for(let i = 0; i < numeros.length; i++){
    numeros[i].addEventListener('click',() => {
        buscar.value =  buscar.value + i;
    })
}

//filtrar as informacoes necessarias e montar na pokedex
function montarPokemon(numeroPokemon) {
  let pokeapi = new pokeApi();
  pokeapi.buscarNaApi(numeroPokemon);
  let infopokemon = new infoPokemon();
  infopokemon.buscarPokemon();
}


botaoBuscar.addEventListener('click', () => {
  numeroPokemon = buscar.value;
  console.log(sorteado[sorteado.length - 1]);
  if(sorteado[sorteado.length - 1] == numeroPokemon){
    alert("Pokemon ja esta sendo exibido na pokedex");
  }else{
    montarPokemon(numeroPokemon);
    sorteado.push(numeroPokemon);
  }
});

//avancar para o proximo Pokemon

proximo.addEventListener('click',() => {
    numeroPokemon = parseInt(numeroPokemon) + 1;
    montarPokemon(numeroPokemon);
})

// retornar para o pokemon anterior

anterior.addEventListener('click',() => {
    numeroPokemon = parseInt(numeroPokemon) - 1;
    montarPokemon(numeroPokemon);
})

sortear.addEventListener('click', () => {
    let i = 1
    while (i > 0) {
      numeroPokemon = Math.floor(Math.random() * 802 + 1);
      i = 0
      for (let j = 0; j < sorteado.length; j++) {
        if (sorteado[j] == numeroPokemon) {
          i++;
        }
      }
    }
      montarPokemon(numeroPokemon);
      sorteado.push(numeroPokemon);
});
