class infoPokemon {

    constructor(){

    }

    buscarPokemon(){
        setTimeout(() => {
            let imagem = document.getElementById('imagem');
            let nome = document.getElementById('nome');
            let altura = document.getElementById('altura');
            let idPokemon = document.getElementById('id');
            let peso = document.getElementById('peso');
            imagem.src = pokemon.sprites.front_default;
            nome.innerHTML = "Nome: " + pokemon.name;
            altura.innerHTML = "Altura: " + (pokemon.height * 10) + " cm";
            peso.innerHTML = "Peso " + (pokemon.weight / 10) +" Kg"
            idPokemon.innerHTML = "ID: " + pokemon.id;
            this.listarEstatisticas();
            this.listarTipos();
            this.fundo();
            console.log(pokemon);
          }, 1500);
    }

    

    listarEstatisticas(){
        let pai = document.getElementById("estatisticas");
        let filho = pai.querySelector("ul");
        let filhoSpan = pai.querySelector("span");
    
        if (filho !== null) {
          filho.parentNode.removeChild(filho);
          filhoSpan.parentNode.removeChild(filhoSpan);
        }
        let div = document.getElementById('estatisticas');
        let span = document.createElement('span');
        span.innerHTML = "Estatisticas";
        div.appendChild(span);
        let ul = document.createElement('ul');
        div.appendChild(ul);
        for(let i = 1; i < pokemon.stats.length ; i++){
          let li = document.createElement('li');  
          li.innerHTML = pokemon.stats[i].stat.name + " " + pokemon.stats[i].base_stat;
          ul.appendChild(li);
        }
    
    }

    

      fundo(){
        switch (pokemon.types[0].type.name) {
          case 'water':
            fundoImagem.id = "fundo-agua";
            break;
          case 'fire':
            fundoImagem.id = "fundo-fogo";
            break;
          case 'ice':
            fundoImagem.id = "fundo-gelo";
            break;
          default:
            fundoImagem.id = "fundo-paisagem";
            break;
        }
        if(pokemon.types[0].type.name == 'water'){
          fundoImagem.id = "fundo-agua";
        }
      }

      listarTipos(){
        let pai = document.getElementById("tipos");
        let filho = pai.querySelector("ul");
        let filhoSpan = pai.querySelector("span");
      
        if (filho !== null) {
          filho.parentNode.removeChild(filho);
          filhoSpan.parentNode.removeChild(filhoSpan);
      
        }
        let div = document.getElementById('tipos');
        let span = document.createElement('span');
        span.innerHTML = "Tipos"
        div.appendChild(span);
        let ul = document.createElement('ul');
        div.appendChild(ul);
        for(let i = 0; i < pokemon.types.length ; i++){
          let li = document.createElement('li');  
          li.innerHTML = pokemon.types[i].type.name;
          ul.appendChild(li);
        }
      
      }
    
}