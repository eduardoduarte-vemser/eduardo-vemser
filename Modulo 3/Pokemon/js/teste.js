
const api = 'https://pokeapi.co/api/v2/pokemon/';

const buscar = document.getElementById('inputDisplay'),
      botaoBuscar = document.getElementById('enviar')
;
let numeroPokemon, pokemon;

let numeros = document.getElementsByClassName('numeros');
let proximo = document.getElementById('proximo');
let anterior = document.getElementById('anterior');
let sortear = document.getElementById('sorteador');
let fundoImagem = document.querySelector('.img-pokemon');
let sorteado = [];
let msg = "";
// habilitar botoes numericos da pokedex
for(let i = 0; i < numeros.length; i++){
    numeros[i].addEventListener('click',() => {
        buscar.value =  buscar.value + i;
    })
}

//buscar um pokemon em especifico na API
// function buscarNaApi(api, numeroPokemon) {
//   fetch(api + numeroPokemon)
//     .then(resposta => resposta.json())
//     .then(data => {
//       pokemon = data;
//     })
//     .catch(erro => {
//       msg = "O pokemon digitado não existe ";
//       alert(msg);
//     });
// }

//filtrar as informacoes necessarias e montar na pokedex
function montarPokemon(numeroPokemon) {
  let pokeapi = new pokeApi();
  pokeapi.buscarNaApi(numeroPokemon);
  let infopokemon = new infoPokemon();
  infopokemon.buscarPokemon();
  // buscarNaApi(api, numeroPokemon);
  // setTimeout(function () {
  //   let imagem = document.getElementById('imagem');
  //   let nome = document.getElementById('nome');
  //   let altura = document.getElementById('altura');
  //   let idPokemon = document.getElementById('id');
  //   let peso = document.getElementById('peso');
  //   imagem.src = pokemon.sprites.front_default;
  //   nome.innerHTML = "Nome: " + pokemon.name;
  //   altura.innerHTML = "Altura: " + (pokemon.height * 10) + " cm";
  //   peso.innerHTML = "Peso " + (pokemon.weight / 10) +" Kg"
  //   idPokemon.innerHTML = "ID: " + pokemon.id;
  //   listarEstatisticas(pokemon);
  //   listarTipos(pokemon);
  //   fundo(pokemon);
  //   console.log(pokemon);
  // }, 1500);
}
// function listarEstatisticas(pokemon){
//     let pai = document.getElementById("estatisticas");
//     let filho = pai.querySelector("ul");
//     let filhoSpan = pai.querySelector("span");

//     if (filho !== null) {
//       filho.parentNode.removeChild(filho);
//       filhoSpan.parentNode.removeChild(filhoSpan);
//     }
//     let div = document.getElementById('estatisticas');
//     let span = document.createElement('span');
//     span.innerHTML = "Estatisticas";
//     div.appendChild(span);
//     let ul = document.createElement('ul');
//     div.appendChild(ul);
//     for(let i = 1; i < pokemon.stats.length ; i++){
//       let li = document.createElement('li');  
//       li.innerHTML = pokemon.stats[i].stat.name + " " + pokemon.stats[i].base_stat;
//       ul.appendChild(li);
//     }

// }

// function fundo(pokemon){
//   switch (pokemon.types[0].type.name) {
//     case 'water':
//       fundoImagem.id = "fundo-agua";
//       break;
//     case 'fire':
//       fundoImagem.id = "fundo-fogo";
//       break;
//     case 'ice':
//       fundoImagem.id = "fundo-gelo";
//       break;
//     default:
//       fundoImagem.id = "fundo-paisagem";
//       break;
//   }
//   if(pokemon.types[0].type.name == 'water'){
//     fundoImagem.id = "fundo-agua";
//   }
// }

// function listarTipos(pokemon){
//   let pai = document.getElementById("tipos");
//   let filho = pai.querySelector("ul");
//   let filhoSpan = pai.querySelector("span");

//   if (filho !== null) {
//     filho.parentNode.removeChild(filho);
//     filhoSpan.parentNode.removeChild(filhoSpan);

//   }
//   let div = document.getElementById('tipos');
//   let span = document.createElement('span');
//   span.innerHTML = "Tipos"
//   div.appendChild(span);
//   let ul = document.createElement('ul');
//   div.appendChild(ul);
//   for(let i = 0; i < pokemon.types.length ; i++){
//     let li = document.createElement('li');  
//     li.innerHTML = pokemon.types[i].type.name;
//     ul.appendChild(li);
//   }

// }

botaoBuscar.addEventListener('click', () => {
  numeroPokemon = buscar.value;
  console.log(sorteado[sorteado.length - 1]);
  if(sorteado[sorteado.length - 1] == numeroPokemon){
    alert("Pokemon ja esta sendo exibido na pokedex");
  }else{
    montarPokemon(numeroPokemon);
    sorteado.push(numeroPokemon);
  }
});

//avancar para o proximo Pokemon

proximo.addEventListener('click',() => {
    numeroPokemon = parseInt(numeroPokemon) + 1;
    montarPokemon(numeroPokemon);
})

// retornar para o pokemon anterior

anterior.addEventListener('click',() => {
    numeroPokemon = parseInt(numeroPokemon) - 1;
    montarPokemon(numeroPokemon);
})

sortear.addEventListener('click', () => {
    let i = 1
    while (i > 0) {
      numeroPokemon = Math.floor(Math.random() * 802 + 1);
      i = 0
      for (let j = 0; j < sorteado.length; j++) {
        if (sorteado[j] == numeroPokemon) {
          i++;
        }
      }
    }
      montarPokemon(numeroPokemon);
      sorteado.push(numeroPokemon);
});