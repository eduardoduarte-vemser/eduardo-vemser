//console.log("Cheguei", "Aqui");

var nomeDaVariavel = "valor";
let nomeDaLet = "ValorLet";
const nomeDaConst = "ValorConst";

var nomeDaVariavel = "Valor3";
nomeDaLet = "ValorLet2";

const nomeDaConst2 = {
    nome: "Marcos",
    idade: 29
};


nomeDaConst2.nome = "Marcos Henrique";

function nomeDaFuncao(){
    //nomeDaVariavel = "valor";
}

//console.log(nomeDaLet);

/*
function somar(){

}
*/

function somar(valor1,valor2 = 1){
    console.log(valor1 + valor2);
}

// somar(3);
// somar(3,2);

// console.log(1 + 1);
// console.log(1 +"1" + 2);
// console.log("1" + "1");

function ondeMoro(cidade){
    console.log(`Moro em ${cidade}. E sou feliz`);
}

//ondeMoro("Porto Alegre");

function fruteira(){
    let texto = "Banana" + "\n" +"Ameixa" + "\n" + "Goiaba" + "\n" +"Pessego" + "\n";

    let newTexto = `
    Banana 
        Ameixa 
            Goiaba 
                Pessego
    `;

    
    console.log(texto);
    console.log(newTexto);
    
    
}

//fruteira();

let eu = {
    nome:"Eduardo",
    idade: 28,
    altura: 1.71
};

function quemSou(pesssoa){
    console.log(`Meu nome é ${pesssoa.nome}, tenho ${pesssoa.idade} anos e ${pesssoa.altura} de altura.`);
};

quemSou(eu);

let funcaoSomarValores = function(a, b){
    return a + b;
};

let add =funcaoSomarValores;
let resultado = add(3,5);
//console.log(resultado);

const{
    nome: n,
    idade: i
} = eu;

//console.log(n, i);

//const array = [1, 3, 4, 8];

//const [n1, n2, n3, n4] = array;

//console.log(n2);

//const [s1, s2, s3, s4] = "nome";

//console.log(s2);

function testarPessoa({nome,idade,altura}){
    console.log(nome,idade,altura);
}

testarPessoa(eu);

