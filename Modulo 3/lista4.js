//exercicio 1
function multiplicar(val, ...valores){
    return valores.map(valor => val * valor);
}
console.log(multiplicar(5,3,4));
console.log(multiplicar(5,3,4,5));

//exercicio 2
function validacao(e){
    let target = e.target;
    let msg = '';
    switch (target.id) {
        case "nome":
            if(target.value == ''){
                msg = "Esse campo não pode ser vazio";
                target.focus();
            }
            break;
        case "email":
            if(target.value.indexOf('@') == -1){
                msg = "Esse campo deve conter @";
                target.focus();
            }
            break;
        default:
            break;
    }
    let erro = document.getElementById(`alerta${target.id}`);
    erro.style.color = "red";
    erro.innerHTML = msg;
}

let vazio = document.getElementsByTagName('input');
for(let i = 0; i < vazio.length ; i++){
    vazio[i].addEventListener('blur', (e) => validacao(e));
}

