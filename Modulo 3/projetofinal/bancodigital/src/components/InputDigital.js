
import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class InputDigital extends Component {

    constructor( props ){
        super( props )
    }

    atualizarInput = (evt) =>{
        this.props.ehDigital(evt)
        
    }

    render(){

        const { is_digital } = this.props
        console.log(is_digital);
        
        return(
            <React.Fragment>
                {console.log(is_digital)
                }
        {is_digital ? <input type="checkbox" defaultChecked={true} onClick={this.atualizarInput}></input>: <input type="checkbox" defaultChecked={false} onClick={this.atualizarInput}></input>}Agencia Digital


            </React.Fragment>

        )
    }
} 

InputDigital.propTypes = {
    ehDigital: PropTypes.func.isRequired,
}