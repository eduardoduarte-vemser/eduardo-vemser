
import React, { Component } from 'react';
import { history } from '../history'
import { logout } from '../auth';

export default class Button extends Component {

    constructor( props ){
        super( props )
    }

    render(){

        const { path } = this.props
        return(
            <React.Fragment>
                
                <button onClick={ path }>Buscar detalhes</button>

            </React.Fragment>

        )
    }
}