
import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import imagemLogin from '../img/logo-banco.png'
import { history } from '../history'
import { logout } from '../auth';

export default class Header extends Component {

    fazerLogout() {
        logout()
        history.push('/login')
    }

    UNSAFE_componentWillMount(){
        
    }
    
    render(){
        return(

            <React.Fragment>
                
                <div className="lista-principal">
                    <img className="img-lista" src={imagemLogin} alt="Banco Digital"/>
                    <div className="borda"></div>
                    <Link className="link-lista" to="/agencias">Agencias</Link>
                    <div className="borda"></div>
                    <Link className="link-lista" to="/clientes">Clientes</Link>
                    <div className="borda"></div>
                    <Link className="link-lista" to="/tipoContas">Tipo de contas</Link>
                    <div className="borda"></div>
                    <Link className="link-lista" to="/conta/clientes">Conta de clientes</Link>
                    <div className="borda"></div>
                    <button onClick={this.fazerLogout.bind( this )}>Logout</button>
                </div>
                <div className="tela">
                    {this.props.children}
                </div>

            </React.Fragment>

        )
    }
}