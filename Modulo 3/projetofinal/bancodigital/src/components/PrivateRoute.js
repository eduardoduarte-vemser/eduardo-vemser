import React from 'react'
import { Route, Redirect } from 'react-router'
import { estaLogado } from '../auth'

const PrivateRoute = props => estaLogado()
    ? <Route { ...props }/>
    : <Redirect to="/login"/>

export default PrivateRoute
