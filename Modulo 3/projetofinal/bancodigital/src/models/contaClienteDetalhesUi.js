import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class ContaClienteDetalhesUi extends Component {

    constructor(props){
        super(props)
    }
    
    
    //  agencia={ agencia } enderecoAgencia={ enderecoAgencia }
    render(){
        const { cliente, clienteConta, contaTipo } = this.props;
        // { id: 1,
        //     codigo: 00001,
        //     tipo: {
        //       id: 1,
        //       nome: 'Corrente'
        //     },
        //     cliente: {
        //       id: 1,
        //       nome: 'Facilitador Vem Ser 3',
        //       cpf: '000.000.000-00',
        //     }
        
            return (
                    <React.Fragment key={cliente.id} >
                        <div className="tela-item"> 
                            <h2>Codigo do cliente: 0000{clienteConta.codigo} </h2> 
                            <h3>Tipo</h3>
                            <p>Conta {contaTipo.nome}</p>
                            <h3>Cliente</h3>
                            <p>Nome: {cliente.nome}</p>
                            <p>CPF: {cliente.cpf}</p>
                        </div> 
                    </React.Fragment>
                    ) 

        
    }
}