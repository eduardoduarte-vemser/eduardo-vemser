import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class AgenciaUi extends Component {

    constructor(props){
        super(props)
    }


    render(){
        const { agencias } = this.props;
            return (
                agencias.map((agencia) => 
                    <React.Fragment key={agencia.id} >
                        <div className="tela-itens"> 
                            <h2>Agencia: 000{agencia.codigo} </h2> 
                            <h3>Nome de Agencia: {agencia.nome}</h3>
                            <Link to={`agencia/${agencia.id - 1}`} ><button>Detalhes</button></Link>
                        </div> 
                    </React.Fragment>
                        ) 
            )

        
    }
}