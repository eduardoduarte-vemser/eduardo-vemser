import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class ContaClientesUi extends Component {

    constructor(props){
        super(props)
    }
    

    render(){
        const { clientes } = this.props;
        
        
            return (
                clientes.map((cliente) => 
                    <React.Fragment key={cliente.id} >
                        <div className="tela-itens"> 
                            <h2>Cliente: {cliente.cliente.nome} </h2> 
                            <Link to={`cliente/${cliente.id - 1}`} ><button>Detalhes</button></Link>
                        </div> 
                    </React.Fragment>
                        ) 
            )

        
    }
}