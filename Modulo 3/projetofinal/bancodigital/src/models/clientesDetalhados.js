import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class ClientesDetalhadosdaUi extends Component {

    constructor(props){
        super(props)
    }

    render(){
        const { cliente, agencia, enderecoAgencia } = this.props;

            return (
                    <React.Fragment key={cliente.id} >
                        <div className="tela-item"> 
                            <h2>Nome do cliente: {cliente.nome} </h2> 
                            <h3>Nome de Agencia: {cliente.cpf}</h3>
                            <h3>Agencia</h3>
                            <p>Codigo: {agencia.codigo}</p>
                            <p>Nome: {agencia.nome}</p>
                            <h3>Enderco</h3>
                            <p>Logradouro: {enderecoAgencia.logradouro}</p>
                            <p>Numero: {enderecoAgencia.numero}</p>
                            <p>Cidade: {enderecoAgencia.cidade}</p>
                            <p>UF: {enderecoAgencia.uf}</p>
                        </div> 
                    </React.Fragment>
                    ) 

        
    }
}