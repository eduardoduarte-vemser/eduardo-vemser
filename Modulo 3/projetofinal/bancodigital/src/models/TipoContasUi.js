import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class TipoContasUi extends Component {

    constructor(props){
        super(props)
    }


    render(){
        const { tipoContas } = this.props;
            return (
                tipoContas.map((tipoConta) => 
                    <React.Fragment key={tipoConta.id} >
                        <div className="tela-itens"> 
                            <h2>Conta {tipoConta.nome} </h2> 
                            <Link to={`tiposConta/${tipoConta.id - 1}`} ><button>Detalhes</button></Link>
                        </div> 
                    </React.Fragment>
                        ) 
            )

        
    }
}