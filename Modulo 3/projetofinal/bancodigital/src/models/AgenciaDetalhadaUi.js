import React, { Component } from 'react'



export default class AgenciaDetalhadaUi extends Component {

    constructor(props){
        super(props)
        
    }

    render(){
        const { agencia, enderecoAgencia, } = this.props;

            
            return (
                    <React.Fragment key={agencia.id} >
                        <div className="tela-item"> 
                            <h2>Agencia: 000{agencia.codigo} </h2> 
                            <h3>Nome de Agencia: {agencia.nome}</h3>
                            <h3>endereco</h3>
                            <p>Logradouro: {enderecoAgencia.logradouro}</p>
                            <p>Numero: {enderecoAgencia.numero}</p>
                            <p>Bairro: {enderecoAgencia.bairro}</p>
                            <p>Cidade: {enderecoAgencia.cidade}</p>
                            <p>UF: {enderecoAgencia.uf}</p>
                            {this.props.children}
                        </div> 
                    </React.Fragment>
                    ) 

        
    }
}

