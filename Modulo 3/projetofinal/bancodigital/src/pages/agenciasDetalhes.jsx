import React, { Component } from 'react';
import * as axios from 'axios';
// import '../style/clientes.css'
import Header from '../components/Header';
import AgenciaDetalhadaUi from '../models/AgenciaDetalhadaUi'
import InputDigital from '../components/InputDigital'

export default class AgenciasDetalhes extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            agencia: "",
            enderecoAgencia:"",
        }

    }
    
    

    componentWillMount() {
        axios.get(`http://localhost:1337${this.props.location.pathname}`, {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({agencia: res.data.agencias,
            enderecoAgencia: res.data.agencias.endereco}
                )
        })
    }

    ehDigital = evt => {
        if(evt.target.checked){
            this.setState({
                is_digital: true
            })
        }else{
            this.setState({
                is_digital: false
            })
        }

    }

    render() {
        const  { agencia, enderecoAgencia, is_digital }= this.state

        
        return(
            <Header>
                <AgenciaDetalhadaUi agencia={ agencia } enderecoAgencia={ enderecoAgencia }>
                    <InputDigital is_digital={is_digital} ehDigital = {this.ehDigital}></InputDigital>
                </AgenciaDetalhadaUi>
            </Header>
        )

    }
}


