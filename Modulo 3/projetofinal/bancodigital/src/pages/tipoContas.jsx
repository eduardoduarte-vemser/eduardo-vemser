import React, { Component } from 'react';
import * as axios from 'axios';
import '../style/agencias.css'
import Header from '../components/Header';
import TipoContasUi from '../models/TipoContasUi'

export default class TipoContas extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            tipoContasFiltrada: [],
            tipoContas: []
        }
    }

    componentWillMount() {
        axios.get('http://localhost:1337/tipoContas', {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                tipoContas: res.data.tipos,
                tipoContasFiltrada: res.data.tipos
            })
        })
    }

    buscarAgencia( evt ) {
        const { tipoContas } =this.state
        const buscar = tipoContas.filter(tipoConta => tipoConta.nome.includes(evt.target.value))
        this.setState({
            tipoContasFiltrada: buscar
        })
    }

    render() {
        const {tipoContas, tipoContasFiltrada } = this.state  
        return(
            <Header>
                <div className="buscar-agencias">
                    <div>
                        Buscar<input placeholder="Nome da Agencia" onChange={this.buscarAgencia.bind( this )}></input>
                    </div>
                </div> 
                <TipoContasUi tipoContas = {tipoContasFiltrada} key={0}/>
            </Header>
        )

    }
}
