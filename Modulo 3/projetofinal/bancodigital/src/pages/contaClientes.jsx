import React, { Component } from 'react';
import * as axios from 'axios';
// import '../style/clientes.css'
import Header from '../components/Header';
import '../style/clientes.css'
import ContaClientesUi from '../models/contaClientesUi'

export default class ContaClientes extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            clientes: [],
            clientesFiltrados: [],
        }

    }

    componentWillMount() {
        axios.get('http://localhost:1337/conta/clientes', {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                clientes: res.data.cliente_x_conta,
                clientesFiltrados: res.data.cliente_x_conta
            },
                )
        })
    }

    buscarAgencia( evt ) {
        const {clientes} = this.state
        const buscar = clientes.filter(cliente => cliente.cliente.nome.includes(evt.target.value))
        this.setState({
            clientesFiltrados: buscar
        })
    }

    render() {
        const  { clientes, clientesFiltrados }= this.state
        let nomeCliente = null , nomeTipo = null, idTipo= null ;
        for (let i = 0; i < clientes.length; i++) {
            idTipo = clientes[i].id
            nomeTipo = clientes[i].tipo.nome
            nomeCliente = clientes[i].cliente.nome
        }
        
        
        return(
            <Header>
                {/* {clientes.map(cliente => cliente.id)} */}
                <div className="buscar-clientes">
                    <div>
                        Buscar<input placeholder="Nome da Agencia" onChange={this.buscarAgencia.bind( this )}></input>
                    </div>
                </div> 
                <ContaClientesUi clientes ={ clientesFiltrados } nomeCliente = { nomeCliente } nomeTipo ={ nomeTipo }></ContaClientesUi>
            </Header>
        )

    }
}