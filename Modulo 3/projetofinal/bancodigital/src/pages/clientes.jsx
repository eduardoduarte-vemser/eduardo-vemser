import React, { Component } from 'react';
import * as axios from 'axios';
// import '../style/clientes.css'
import Header from '../components/Header';
import '../style/clientes.css'
import ClienteUi from '../models/ClienteUI'

export default class Clientes extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            clientes: [],
            agencias: [],
            clientesFiltrados: []
        }

    }

    componentWillMount() {
        axios.get('http://localhost:1337/clientes', {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                clientes: res.data.clientes,
                clientesFiltrados: res.data.clientes
            },
                )
        })
    }

    buscarAgencia( evt ) {
        const {clientes} = this.state
        const buscar = clientes.filter(cliente => cliente.nome.includes(evt.target.value))
        this.setState({
            clientesFiltrados: buscar
        })
    }

    render() {
        const  { clientesFiltrados }= this.state
        return(
            <Header>
                <div className="buscar-clientes">
                    <div>
                        Buscar<input placeholder="Nome da Agencia" onChange={this.buscarAgencia.bind( this )}></input>
                    </div>
                </div> 
                <ClienteUi clientes ={ clientesFiltrados }></ClienteUi>
            </Header>
        )

    }
}