import React, { Component } from 'react';
import * as axios from 'axios';
// import '../style/clientes.css'
import Header from '../components/Header';
import ClientesDetalhados from '../models/clientesDetalhados';

export default class ClientesDetalhes extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            cliente: "",
            agencia:"",
            enderecoAgencia: ""
        }

    }

    componentWillMount() {
        axios.get(`http://localhost:1337${this.props.location.pathname}`, {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                cliente: res.data.cliente,
                agencia: res.data.cliente.agencia,
                enderecoAgencia: res.data.cliente.agencia.endereco
                
            },
                )
        })
    }

    render() {
        const  { cliente, agencia, enderecoAgencia }= this.state
        return(
            <Header>
                <ClientesDetalhados cliente = {cliente} agencia={ agencia } enderecoAgencia={ enderecoAgencia }/>
            </Header>
        )

    }
}