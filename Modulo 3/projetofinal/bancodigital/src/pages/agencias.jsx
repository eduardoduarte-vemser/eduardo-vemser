import React, { Component } from 'react';
import * as axios from 'axios';
import '../style/agencias.css'
import Header from '../components/Header';
import AgendaUi from '../models/AgenciaUi'

export default class Agencias extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            agenciaFiltrada: [],
            agencias: [],
            is_digital: false
        }
    }

    componentWillMount() {
        axios.get('http://localhost:1337/agencias', {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                agencias: res.data.agencias,
                agenciaFiltrada: res.data.agencias
            })
        })
    }

    ehDigital = evt => {
        if(evt.target.checked){
            this.setState({
                is_digital: true
            })
        }else{
            this.setState({
                is_digital: false
            })
        }

    }

    buscarAgencia( evt ) {
        const {agencias, agenciaFiltrada} = this.state
        const buscar = agencias.filter(agencia => agencia.nome.includes(evt.target.value))
        this.setState({
            agenciaFiltrada: buscar
        })
        
    }

    render() {
        const {agencias, agenciaFiltrada, is_digital} = this.state  
        console.log(this.state);
        
        return(
            <Header>
                <div className="buscar-agencias">
                    <div>
                        Buscar<input placeholder="Nome da Agencia" onChange={this.buscarAgencia.bind( this )}></input>
                        <div>
                            <input type="checkbox"></input> Somente Agencias digitais
                        </div>
                    </div>
                </div> 
                <AgendaUi agencias = {agenciaFiltrada} key={0}/>
            </Header>
        )

    }
}
