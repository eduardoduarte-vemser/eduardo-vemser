import React, { Component } from 'react';
import * as axios from 'axios';
import '../style/login.css'
import imagemLogin from '../img/logo-banco.png'

export default class Login extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            email: '',
            senha: ''
        }
        this.atualizaInput= this.atualizaInput.bind( this )
    }

    atualizaInput( e ) {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    
    logar( e ) {
        e.preventDefault();
        const { senha, email} = this.state
        if( senha && email ) {
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            }).then( resp =>{
                localStorage.setItem( 'Authorization', resp.data.token );
                localStorage.setItem( 'Username', email );
                localStorage.setItem( 'Senha', senha );
                this.props.history.push( '/' )
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                
                
                <img className="img-login" src={imagemLogin} alt="Banco Digital"/>
                <div className="login">
                    <div className="input-login">
                        <input type="text" name="email" placeholder="Email " onChange={ this.atualizaInput}/>
                        <input type="password" name="senha" placeholder="Senha" onChange={ this.atualizaInput}/>
                    </div>
                    <button onClick={ this.logar.bind( this ) } >Logar</button>
                </div>
                
            </React.Fragment>
        )
    }
}