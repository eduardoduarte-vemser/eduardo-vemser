import React, { Component } from 'react';
import * as axios from 'axios';
// import '../style/clientes.css'
import Header from '../components/Header';

export default class TipoContaDetalhes extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            tipoConta: ""
        }

    }

    componentWillMount() {
        let index = this.props.location.pathname.replace('/tipoContas/','');
        axios.get(`http://localhost:1337${this.props.location.pathname}`, {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                tipoConta: res.data.tipos
            })
        })
        
    }

    render() {
        const  { tipoConta }= this.state
        return(
            <React.Fragment>
                <Header>
                    <div className="tela-item"> 
                        <h2>Id: { tipoConta.id } </h2>
                        <h2>Conta { tipoConta.nome } </h2> 
                    </div> 
                </Header>
                <h1></h1>
            </React.Fragment>
        )

    }
}