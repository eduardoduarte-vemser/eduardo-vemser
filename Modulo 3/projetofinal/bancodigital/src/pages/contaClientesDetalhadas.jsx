import React, { Component } from 'react';
import * as axios from 'axios';
// import '../style/clientes.css'
import Header from '../components/Header';
import ContaClienteDetalhesUi from '../models/contaClienteDetalhesUi'

export default class ContaClientesDetalhadas extends Component {
    constructor( props ) {
        super( props )
        this.state = {
            cliente: "",
            clienteConta: "",
            contaTipo: ""
        }

    }

    componentWillMount() {
        let index = this.props.location.pathname.replace('/conta/cliente/','');
        axios.get(`http://localhost:1337${this.props.location.pathname}`, {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                cliente: res.data.conta.cliente,
                clienteConta: res.data.conta,
                contaTipo: res.data.conta.tipo
            })
        })
    }

    render() {
        const  { cliente, contaTipo, clienteConta }= this.state
        return(
            <Header>
                <ContaClienteDetalhesUi cliente ={ cliente } contaTipo={ contaTipo } clienteConta={ clienteConta } />
            </Header>
        )

    }
}