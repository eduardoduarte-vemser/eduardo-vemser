import React, { Component } from 'react';
import './App.css';
import { history } from './history'
import Login from './pages/login';
import Clientes from './pages/clientes'
import ClientesDetalhes from './pages/clientesDetalhes'
import AgenciasDetalhes from './pages/agenciasDetalhes'
import Home from './pages/home';
import Agencias from './pages/agencias';
import TipoContas from './pages/tipoContas'
import { Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import ContaClientes from './pages/contaClientes'
import ContaClientesDetalhadas from './pages/contaClientesDetalhadas'
import TipoContaDetalhes from './pages/tipoContaDetalhes'

export default class App extends Component {
  constructor ( props ) { // eslint-disable-line no-useless-constructor
    super( props )
  }

  render() {
    return (
      <div className="App">
        <Router history = { history }>
            <Switch>
                <Route  path="/login" component={ Login } />
                <PrivateRoute exact path="/"  component={ Home } />
                <PrivateRoute path="/agencias" component={ Agencias } />
                <PrivateRoute exact path="/clientes" component={ Clientes } />
                <PrivateRoute exact path="/cliente/:id" component={ ClientesDetalhes } />
                <PrivateRoute exact path="/agencia/:id" component={ AgenciasDetalhes } />
                <PrivateRoute exact path="/tipoContas" component={ TipoContas } />
                <PrivateRoute exact path="/conta/clientes" component={ ContaClientes } />
                <PrivateRoute exact path="/conta/cliente/:id" component={ ContaClientesDetalhadas } />
                <PrivateRoute exact path="/tiposConta/:id" component={ TipoContaDetalhes } />
            </Switch>
        </Router>
      </div>
    );
  }
}