

//Exercicio 1 

let circulo = {
    raio: 10,
    tipoCalculo: "C"
}

function calcularCirculo (circulo){
    return Math.ceil(circulo.tipoCalculo == "A" ? Math.PI * Math.pow(circulo.raio, 2) : Math.PI * circulo.raio);
}

//console.log(calcularCirculo(circulo));

function naoBissexto(ano){
    if ((ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0))){
        return false;
    }else{
        return true;
    }
}

let array = [1,56,4.34,6,-2];
function somarPares(umArray){
    let soma = 0.0;
    for(let i = 0 ; i < array.length ;i++){
        if(i % 2 == 0){
            soma += array[i];
        }
    }

    return soma;
}

// function adicionar(numero){
//     function adicionar2(numero2){
//         return numero + numero2
//     }
//     return adicionar2;
// }

let adicionar = op1 => op2 => op1 + op2;

adicionar(2)(3);
console.log(adicionar(2)(3));

// function imprimirBRL(numero){
//     return numero.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
// }

function imprimirBRL(numero){
    let str = numero.toString();
    let resultado = str.split(".");
    if(resultado[1].length > 2){
        [n1,n2,n3] = resultado[1];
        let num = parseFloat(numero.toFixed(2));
            if(n3 < 6){
                return (num + 0.01).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
            }
        return num.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    }
    let num = parseFloat(numero.toFixed(2));
    return num.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    
}
console.log(imprimirBRL(10.95));
