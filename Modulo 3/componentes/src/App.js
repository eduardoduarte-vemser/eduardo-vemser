import React, { Component } from 'react';
import './App.css';

// import CompA, { CompB } from './componentes/ExemploComponenteBasico'
import Membros from './componentes/Membros'

class App extends Component {
  constructor( props ) {
    super( props );
  }

  render() {
    return (
      <div className="App">
        <Membros nome="Eduardo" sobreNome="Duarte"/>
        <Membros nome="Eduardo" sobreNome="Duarte"/>
        {/* <CompA/>
        <CompB/> */}

      </div>
    );
  }
}

export default App;
