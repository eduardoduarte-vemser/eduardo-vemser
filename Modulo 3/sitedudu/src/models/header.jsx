import React, { Component } from 'react';
import imagem from '../img/logo-dbc-topo.png'
import '../css/banner.css'
import '../App.css';
import '../css/reset.css'
import '../css/header.css'
import '../css/grid.css'
import '../css/footer.css'
import '../css/buttons.css'
import '../css/box.css'
import { Link } from 'react-router-dom';

export default class HeaderUi extends Component {

    render() {
        return (
            <React.Fragment>
                <header className="main-header">
                    <nav className="container clearfix">
                    <a className="logo" href="#">
                        <img src={imagem} alt="DBC Company" title="DBC Company"/>
                    </a>
                    <label className="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox"/>
                    <ul className="clearfix">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/sobre">Sobre nós</Link>
                        </li>
                        <li>
                            <Link to="/servicos">Serviços</Link>
                        </li>
                        <li>
                            <Link to="/contato">Contato</Link>
                        </li>
                    </ul>
                </nav>
            </header>
            </React.Fragment>
            
        )
    }

}