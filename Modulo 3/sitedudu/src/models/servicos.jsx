import React, { Component } from 'react';

export default class ServicosUi extends Component {

    render() {
        const { nome, nomeimagem, conteudo } = this.props 
        return (
            <React.Fragment>
                
                    <div className="col col-12 col-md-6 col-lg-3">
                        <article className="box">
                            <div>
                                <img src={require('../img/'+nomeimagem+'.png')} alt=""/>
                            </div>
                            <h4>{ nome }</h4>
                            <p>
                                { conteudo }
                            </p>
                            <a className="button button-green" href="#">Saiba mais</a>
                        </article>
                    </div>
            </React.Fragment>
            
        )
    }
}

