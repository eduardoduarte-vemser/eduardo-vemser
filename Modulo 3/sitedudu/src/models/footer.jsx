import React, { Component } from 'react';
import '../css/banner.css'
import '../App.css';
import '../css/reset.css'
import '../css/header.css'
import '../css/grid.css'
import '../css/footer.css'
import '../css/buttons.css'
import '../css/box.css'
import { Link } from 'react-router-dom';

export default class FooterUi extends Component {

    render() {
        return ( 
            <React.Fragment>
                <footer className="main-footer">
                    <div className="container">
                        <nav>
                            <ul>
                                <li>
                                    <Link to="/" >Home</Link>
                                </li>
                                <li>
                                    <Link to="/sobre" >Sobre nós</Link>
                                </li>
                                <li>
                                    <Link to="/servicos">Serviços</Link>
                                </li>
                                <li>
                                    <Link to="/contato">Contato</Link>
                                </li>
                            </ul>
                        </nav>
                        <p>
                            &copy; Copyright DBC Company - 2019
                        </p>
                    </div>
                </footer>
            </React.Fragment>
        )
    }
}