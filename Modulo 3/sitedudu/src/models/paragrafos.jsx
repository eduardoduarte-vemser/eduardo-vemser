import React, { Component } from 'react';

export default class ParagrafosUi extends Component {

    render() {
        const  { classes, h2, conteudo } = this.props;
        return ( 
            
            <React.Fragment>
                
                <div className= { classes }>
                    <h2>{ h2 }</h2>
                    <p>
                        { conteudo }
                    </p>                
                </div>
            </React.Fragment>
            
        )
    }
}