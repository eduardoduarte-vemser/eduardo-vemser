import React from 'react';
import HeaderUi from '../models/header';
import ServicosUi from '../models/servicos';
import ParagrafosUi from '../models/paragrafos';
import FooterUi from '../models/footer';

 const Sobre = () => 
    <div>
        <HeaderUi />
        <section class="container offset-top">
            <div class="row">
                <article>
                    <div class=" col col-6 col-md-3">
                        <div class="img-sobre">
                            <img src={require("../img/logo-dbc-topo.png")} alt="DBC Company" title="DBC Company"/>
                        </div>
                    </div>
                    <div class="col col-6 col-md-9">
                        <h2>Título</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed mollitia harum eos similique dolorem, tenetur in labore fuga, pariatur ex facere excepturi illo vitae nulla. Maxime enim facere quos mollitia.
                        </p>
                    </div>
                </article>
            </div>
            <div class="row">
                <article>
                    <div class="col col-6 col-md-9">
                        <h2>Título</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed mollitia harum eos similique dolorem, tenetur in labore fuga, pariatur ex facere excepturi illo vitae nulla. Maxime enim facere quos mollitia.
                        </p>
                    </div>
                    <div class=" col col-6 col-md-3">
                        <div class="img-sobre">
                            <img src={require("../img/logo-dbc-topo.png")} alt="DBC Company" title="DBC Company"/>
                        </div>
                    </div>
                </article>
            </div>
            <div class="row">
                <article>
                    <div class=" col col-6 col-md-3">
                        <div class="img-sobre">
                            <img src={require("../img/logo-dbc-topo.png")} alt="DBC Company" title="DBC Company"/>
                        </div>
                    </div>
                    <div class="col col-6 col-md-9">
                        <h2>Título</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed mollitia harum eos similique dolorem, tenetur in labore fuga, pariatur ex facere excepturi illo vitae nulla. Maxime enim facere quos mollitia.
                        </p>
                    </div>
                </article>
            </div>
            <div class="row">
                <article>
                    <div class="col col-6 col-md-9">
                        <h2>Título</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed mollitia harum eos similique dolorem, tenetur in labore fuga, pariatur ex facere excepturi illo vitae nulla. Maxime enim facere quos mollitia.
                        </p>
                    </div>
                    <div class=" col col-6 col-md-3">
                        <div class="img-sobre">
                            <img src={require("../img/logo-dbc-topo.png")} alt="DBC Company" title="DBC Company"/>
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <FooterUi />
    </div>


export default Sobre;