import React from 'react';
import HeaderUi from '../models/header';
import ServicosUi from '../models/servicos';
import ParagrafosUi from '../models/paragrafos';
import FooterUi from '../models/footer';

 const Servicos = () => 
    <div>
        <HeaderUi />
        <div className="row offset-top">
            <ServicosUi nome="Titulo1" conteudo="Conteudo1" nomeimagem="Agil"/>
            <ServicosUi nome="Titulo2" conteudo="Conteudo2" nomeimagem="Sustain"/>
            <ServicosUi nome="Titulo3" conteudo="Conteudo4" nomeimagem="Software-Builder"/>
            <ServicosUi nome="Titulo5" conteudo="Conteudo6" nomeimagem="Smartsourcing"/>
            <ServicosUi nome="Titulo3" conteudo="Conteudo4" nomeimagem="Software-Builder"/>
            <ServicosUi nome="Titulo5" conteudo="Conteudo6" nomeimagem="Smartsourcing"/>
        </div>
        <div className="row offset-top">
                <div className="col col-12">
                    <article className="box">
                        <h4>Título</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem, culpa eius. Nam, odit veniam ab dicta aliquam quam ullam sit et cumque tempora beatae blanditiis fuga magnam enim totam aut!
                        </p>
                        <a className="button button-green" href="#">Saiba mais</a>
                    </article>
                </div>
            </div>
        <FooterUi />
    </div>


export default Servicos;