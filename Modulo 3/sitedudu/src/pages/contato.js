import React from 'react';
import HeaderUi from '../models/header';
import ServicosUi from '../models/servicos';
import ParagrafosUi from '../models/paragrafos';
import FooterUi from '../models/footer';
import FormUi from '../models/FormUI';
import '../css/banner.css'
import '../App.css';
import '../css/reset.css'
import '../css/header.css'
import '../css/grid.css'
import '../css/footer.css'
import '../css/buttons.css'
import '../css/box.css'
import '../css/form.css'

 const Contato = () => 
    <div>
        <HeaderUi />
        <section className="container">
            <div className="row">
                <FormUi />
                <div className="col col-12 col-md-6 ">
                    <iframe className="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760873!2d-51.17087028557351!3d-30.01619288189256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576786889182!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe>
                </div>
            </div>
        </section>
        <FooterUi />
    </div>


export default Contato;