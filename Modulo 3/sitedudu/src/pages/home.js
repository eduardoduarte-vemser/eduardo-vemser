import React, { Component } from 'react';
import '../css/banner.css'
import '../App.css';
import '../css/reset.css'
import '../css/header.css'
import '../css/grid.css'
import '../css/footer.css'
import '../css/buttons.css'
import '../css/box.css'
import HeaderUi from '../models/header';
import ServicosUi from '../models/servicos';
import ParagrafosUi from '../models/paragrafos';
import FooterUi from '../models/footer';



class Home extends Component {
  
  constructor( props ){
    super( props )
  }

  render() {
    return (
      <div className="App">
        <HeaderUi/>
        <section className="main-banner">
          <article>
            <h1>Vem Ser DBC</h1>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cupiditate obcaecati, repudiandae, consectetur earum eaque sed magnam soluta quasi aperiam facilis laudantium necessitatibus, inventore consequatur! Ipsam nihil saepe cumque quae nobis!
            </p>
            <a className="button button-green" href="#">Saiba mais</a>
          </article>-
        </section>
        <section className="container">
          <div className="row">
            <ParagrafosUi classes = "col col-12 col-md-7" h2="Titulo 1" conteudo="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Provident fugiat perspiciatis modi perferendis, esse laudantium odit? Ea optio tempora praesentium? Perferendis eaque suscipit doloremque assumenda voluptate deleniti unde porro enim."/> 
            <ParagrafosUi classes = "col col-12 col-md-5" h2="Titulo 2" conteudo="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Provident fugiat perspiciatis modi perferendis, esse laudantium odit? Ea optio tempora praesentium? Perferendis eaque suscipit doloremque assumenda voluptate deleniti unde porro enim."/>
          </div>
          <div className="row">
            <ServicosUi nome="Titulo1" conteudo="Conteudo1" nomeimagem="Agil"/>
            <ServicosUi nome="Titulo2" conteudo="Conteudo2" nomeimagem="Sustain"/>
            <ServicosUi nome="Titulo3" conteudo="Conteudo4" nomeimagem="Software-Builder"/>
            <ServicosUi nome="Titulo5" conteudo="Conteudo6" nomeimagem="Smartsourcing"/>
          </div>
        </section>
        <footer class="main-footer">
            <FooterUi/>
        </footer>
      </div>
    );
  }
}

export default Home;
