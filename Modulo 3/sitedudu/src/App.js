
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import Home from './pages/home';
import Sobre from './pages/sobre';
import Contato from './pages/contato';
import Servicos from './pages/servicos';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home }/>
        <Route path="/sobre" component={ Sobre } />
        <Route path="/servicos" component={ Servicos } />
        <Route path="/contato" component={ Contato } />
      </Router>
    )
  }
}