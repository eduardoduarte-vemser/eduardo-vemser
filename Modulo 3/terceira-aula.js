function criarSanduiche( pao,recheio,queijo ){
    console.log(`Seu sanduiche tem o ${pao} com recheio de ${recheio} e queijo ${queijo}`);
}

const ingredientes = ['3 queijos', 'Frango', 'Cheddar'];

criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor))
}

//receberValoresIndefinidos(1, 2, 3, 4);

console.log(... "Eduardo");

//Window or Document

let inputTeste = document.getElementById('campoTeste');
inputTeste.addEventListener('blur', () => {
    console.log("Obrigado");
});