package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
public class ClienteEntity {
	@SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
	@Id
	@GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_CLIENTE")
	private Integer id_cliente;
	private String nome;
	
	public Integer getId() {
		return id_cliente;
	}
	public void setId(Integer id) {
		this.id_cliente = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
