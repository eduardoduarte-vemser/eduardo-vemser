package br.com.dbccompany.Cartoes;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaCredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;

public class Main {	
	public static void main( String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Eduardo");
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Loja DBC");
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			CredenciadorEntity cred = new CredenciadorEntity();
			cred.setNome("Credito Hoje");
			List<CredenciadorEntity> creditos = new ArrayList<>();
			creditos.add(cred);
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("DBC_Card");
			bandeira.setTaxa(0.03);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Pedro");
			emissor.setTaxa(0.05);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setChip(1);
			cartao.setVencimento("30/01/2020");
			cartao.setBandeira(bandeira);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor);
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setData_compra("12/01/2020");
			lancamento.setDescricao("camiseta do inter");
			lancamento.setCartao(cartao);
			lancamento.setEmissor(emissor);
			lancamento.setLoja(loja);
			lancamento.setValor(120.00);

			LojaCredenciadorEntity loja_cred = new LojaCredenciadorEntity();
			loja_cred.setCredenciador(creditos);
			loja_cred.setLoja(lojas);
			loja_cred.setTaxa(0.60);
			session.save(cliente);
			session.save(loja);
			session.save(cred);
			session.save(cartao);
			session.save(bandeira);
			session.save(emissor);
			session.save(loja_cred);
			session.save(lancamento);
			LancamentoEntity valorLancamento = (LancamentoEntity) session.get(LancamentoEntity.class, 1);
			LojaCredenciadorEntity buscaCred = (LojaCredenciadorEntity) session.get(LojaCredenciadorEntity.class, 1) ;
			EmissorEntity buscaEmissor = (EmissorEntity) session.get(EmissorEntity.class, 1);
			BandeiraEntity buscaBandeira = (BandeiraEntity) session.get(BandeiraEntity.class, 1);
			double taxaCred = buscaCred.getTaxa() * valorLancamento.getValor();
			double taxaEmissor = buscaEmissor.getTaxa() * valorLancamento.getValor();
			double taxaBandeira = buscaBandeira.getTaxa() * valorLancamento.getValor();
	
			transaction.commit();
			System.out.println("Fatia Credenciador: "+taxaCred+"\nFatia Emissor: "+taxaEmissor+"\nFatia Bandeira: "+taxaBandeira);

		}catch (Exception e) {
			if( transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
		
		
	}

}
