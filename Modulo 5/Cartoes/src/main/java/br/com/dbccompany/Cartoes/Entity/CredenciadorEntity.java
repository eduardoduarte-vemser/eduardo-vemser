package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CREDENCIADOR")
public class CredenciadorEntity {

	@SequenceGenerator(allocationSize = 1, name = "CREDENCIADOR_SEQ", sequenceName = "CREDENCIADOR_SEQ")
	@Id
	@GeneratedValue(generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE)
	
	private Integer id_credenciador;
	private String nome;
	
	@ManyToMany( mappedBy = "credenciador" )
	private List<LojaCredenciadorEntity> lojaCredenciador = new ArrayList<>();
	

	public Integer getId_credenciador() {
		return id_credenciador;
	}
	public void setId_credenciador(Integer id_credenciador) {
		this.id_credenciador = id_credenciador;
	}
	public List<LojaCredenciadorEntity> getLojaCredenciador() {
		return lojaCredenciador;
	}
	public void setLojaCredenciador(List<LojaCredenciadorEntity> lojaCredenciador) {
		this.lojaCredenciador = lojaCredenciador;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
