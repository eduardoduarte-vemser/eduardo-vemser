package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LANCAMENTO")

public class LancamentoEntity {
	
	@SequenceGenerator(allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName = "LANCAMENTO_SEQ")
	@Id
	@GeneratedValue(generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id_lancamento;
	private Double valor;
	private String data_compra;
	private String descricao;
	
	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_CARTAO")
	private CartaoEntity cartao;
	
	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_LOJA")
	private LojaEntity loja;
	
	
	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_EMISSOR")
	private EmissorEntity emissor;


	public Integer getLancamento() {
		return id_lancamento;
	}


	public void setLancamento(Integer lancamento) {
		this.id_lancamento = lancamento;
	}


	public Double getValor() {
		return valor;
	}


	public void setValor(Double valor) {
		this.valor = valor;
	}


	public String getData_compra() {
		return data_compra;
	}


	public void setData_compra(String data_compra) {
		this.data_compra = data_compra;
	}


	public CartaoEntity getCartao() {
		return cartao;
	}


	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}


	public LojaEntity getLoja() {
		return loja;
	}


	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}


	public EmissorEntity getEmissor() {
		return emissor;
	}


	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}


	public Integer getId_lancamento() {
		return id_lancamento;
	}


	public void setId_lancamento(Integer id_lancamento) {
		this.id_lancamento = id_lancamento;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
