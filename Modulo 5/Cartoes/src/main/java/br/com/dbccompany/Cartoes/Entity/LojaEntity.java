package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOJA")

public class LojaEntity {
	@SequenceGenerator(allocationSize = 1, name = "LOJA_SEQ", sequenceName = "LOJA_SEQ")
	@Id
	@GeneratedValue(generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_LOJA")
	private Integer id_loja;
	private String nome;
	
	@ManyToMany( mappedBy = "loja" )
	private List<LojaCredenciadorEntity> lojaCredenciador = new ArrayList<>();
	
	
	
	public Integer getId_loja() {
		return id_loja;
	}
	public void setId_loja(Integer id_loja) {
		this.id_loja = id_loja;
	}
	public List<LojaCredenciadorEntity> getLojaCredenciador() {
		return lojaCredenciador;
	}
	public void setLojaCredenciador(List<LojaCredenciadorEntity> lojaCredenciador) {
		this.lojaCredenciador = lojaCredenciador;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
