package br.com.dbccompany.Lotr.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ITEM")
public class ItemEntity {

	@SequenceGenerator(allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
	@Id
	@GeneratedValue(generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id_item;
	private String descricao;
	
	@ManyToMany(mappedBy = "itens")
	private List<InventarioXItem> inventarioXItem = new ArrayList<>();
	
	public Integer getId_item() {
		return id_item;
	}
	public void setId_item(Integer id_item) {
		this.id_item = id_item;
	}
	public List<InventarioXItem> getInventarioXItem() {
		return inventarioXItem;
	}
	public void setInventarioXItem(List<InventarioXItem> inventarioXItem) {
		this.inventarioXItem = inventarioXItem;
	}
	public Integer getId() {
		return id_item;
	}
	public void setId(Integer id) {
		this.id_item = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
