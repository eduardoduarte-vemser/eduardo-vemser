public class TadutorParaIngles implements Tradutor{
  
    public String taduzir( String textoEmPortugues ) {
        switch(textoEmPortugues) {
            case "Sim":
                return "Yes";
            case "Obrigado":
            case "Obrigada":
                return "Thank you";
            default:
                return null;
        }
        
        
    }
}
