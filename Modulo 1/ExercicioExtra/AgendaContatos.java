import java.util.*;
public class AgendaContatos{
   
    private HashMap<String, String> contatos;
    
    
    public AgendaContatos() {
        this.contatos = new LinkedHashMap<>();
    }
    
    public void adicionar( String nome, String telefone ){
        this.contatos.put( nome, telefone );
    }
    
    public String consultar( String nome ){
        return this.contatos.get(nome);
    }
    
    public String consultarPorTelefone( String telefone ){
        for( HashMap.Entry<String, String> par : this.contatos.entrySet() ){
            if( par.getValue().equals(telefone)) {
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv() {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for ( HashMap.Entry<String, String> par : contatos.entrySet() ){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave,valor,separador);
            builder.append(contato);
        }
        return builder.toString();
    }
    
}
