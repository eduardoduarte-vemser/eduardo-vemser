public class Elfo extends Personagem{
    
    private int indiceFlecha;
    private Status status;
    private static int qtdElfos;
    
    {
        indiceFlecha = 0;
    }
    
    //private Item flecha = new Item(2, "Flecha");
    //private Item arco = new Item(1, "Arco");
    

    public Elfo( String nome ){
        super(nome);
        this.vida = 100.0;
        this.itens.adicionar(new Item(2, "Flecha"));
        this.itens.adicionar(new Item(1, "Arco"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos() {
           return Elfo.qtdElfos;
    }

    public Item getFlecha(){
        return this.itens.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    public boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha( Dwarf dwarf ){
        if( podeAtirarFlecha()){
            getFlecha().setQuantidade( this.getQtdFlecha()-1 );
            //this.experiencia = experiencia + 1;
            dwarf.dano();
            this.aumentarXp();
            this.dano();
        }
    }
    
    public String imprimirResumo(){
        return "Elfo";
    }
    
}
