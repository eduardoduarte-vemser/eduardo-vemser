

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {

    
    @Test
    public void calcularMediaInventarioVzio(){
        Inventario itens = new Inventario(11);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertTrue(Double.isNaN(estatistica.calcularMedia()));
    }
    
    @Test
    public void calcularApenaUmItem(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(2, "Espada");
        itens.adicionar(espada);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertEquals(2,estatistica.calcularMedia(),0.01);
    }
    
    @Test
    public void calcularApenasComDoisItem(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(2, "Espada");
        Item arco = new Item(8, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertEquals(5,estatistica.calcularMedia(),0.01);
    }
    
    
    @Test
    public void calcularMediaNumeroDouble(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(5, "Espada");
        Item arco = new Item(10, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertEquals(7.5,estatistica.calcularMedia(),0.01);
    }
    
    @Test
    public void retornaQuantidadeEmOrdem(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(5, "Espada");
        Item arco = new Item(10, "Arco");
        Item arc2 = new Item(7, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(arc2);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertArrayEquals(new int[] {5,7,10},estatistica.ordenarArrayPorQuantidade());
    }
    
    @Test
    public void calcularMedianaPar(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(5, "Espada");
        Item arco = new Item(10, "Arco");
        Item arc3 = new Item(8, "Arco");
        Item arc2 = new Item(7, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(arc2);
        itens.adicionar(arc3);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertEquals(7.5,estatistica.calcularMediana(),0.01);
    }
    
    @Test
    public void calcularMedianaImpar(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(5, "Espada");
        Item arco = new Item(10, "Arco");
        Item arc3 = new Item(8, "Arco");
        Item arc2 = new Item(7, "Arco");
        Item arc4 = new Item(11, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(arc2);
        itens.adicionar(arc3);
        itens.adicionar(arc4);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertEquals(8,estatistica.calcularMediana(),0.01);
    }
    
    @Test
    public void itensComQtdAcimaDaMedia(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(5, "Espada");
        Item arco = new Item(8, "Arco");
        Item arc2 = new Item(9, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(arc2);
        EstatisticasInventario estatistica = new EstatisticasInventario(itens);
        assertEquals(2,estatistica.qtdItensAcimaDaMedia());
    }
    
    
    
}
