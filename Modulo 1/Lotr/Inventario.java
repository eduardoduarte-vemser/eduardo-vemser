import java.util.*;

public class Inventario {
    
   private ArrayList<Item> slots;
   
   public Inventario(int quantidade){
       this.slots =new ArrayList<>(quantidade);
   }
   
   public ArrayList<Item> getSlots(){
       
       return this.slots;
       
   }
   
   public ArrayList<Item> inverter(){
       ArrayList<Item> invertido = new ArrayList<>(this.slots.size());
       for(int i = 0 ; i < this.slots.size() ; i++){
           invertido.add(this.slots.get(this.slots.size() - i - 1));
       }
       return invertido;
       
   }
    
   public void adicionar( Item item ){
        this.slots.add(item);
   }
   
   public ArrayList<Item> unir( Inventario inventarioNovo ){
        for(Item item : inventarioNovo.getSlots()) {
            this.adicionar(item);
        }
       return this.slots;
   }
   
   public ArrayList<Item> diferenciar( Inventario inventarioNovo ){   
        for(Item itemNovo : inventarioNovo.getSlots()) {
            boolean contem = false;
            for(Item item : this.getSlots()) {
                if(itemNovo.getDescricao() == item.getDescricao()){
                    contem = true;
                }
            }
            if(!contem) {
                this.adicionar(itemNovo);
            }
        }
       return this.slots;
   }
   
   public ArrayList<Item> cruzar( Inventario inventarioNovo ){   
        for(Item itemNovo : inventarioNovo.getSlots()) {
            for(Item item : this.getSlots()) {
                if(itemNovo.getDescricao() == item.getDescricao()){
                    item.setQuantidade(itemNovo.getQuantidade() + itemNovo.getQuantidade());
                }
            }
        }
       return this.slots;
   }
   
   public Item obter( int posicao ){
       if(posicao >= this.slots.size()){
           return null;
       }
       return this.slots.get(posicao);
   }
   
   public Item busca( String descricao ){
       for( Item itemAtual : this.slots ) {
           boolean encontrei = itemAtual.getDescricao().equals(descricao);
           if( encontrei ){
               return itemAtual;
           }
       }
       return null;
   }
   
   public void remover(Item item){
       this.slots.remove(item);
   }
   
   public String getDescricoes(){
       StringBuilder descricao = new StringBuilder();
       for( int i = 0 ; i < this.slots.size() ; i++ ){
           Item item = this.slots.get(i);
           if( item != null ){
               descricao.append(item.getDescricao());
               descricao.append(",");
           }
       }
       
       return (descricao.length() > 0 ? descricao.substring( 0, (descricao.length() - 1 )) : descricao.toString());
   }
   
   public Item getItemComMaisQuantidade(){
       int indice = 0, maiorQuantidade = 0;
       
       for (int i = 0 ; i < this.slots.size() ; i++){
           Item item = this.slots.get(i);
           if( item.getQuantidade() > maiorQuantidade) {
               maiorQuantidade = item.getQuantidade();
               indice = i;
           }
       }
       return this.slots.size() > 0 ? this.slots.get(indice) : null;
       
   }
   
   public void ordenarItens(){
       this.ordenarItens(TipoOrdenacao.ASC);
        /*Item aux;
        for(int i = 0 ; i < this.slots.size() ; i++){
           for(int j = 0 ; j < this.slots.size(); j++){
               if(this.slots.get(i).getQuantidade() < this.slots.get(j).getQuantidade()){
                   aux = this.slots.get(i);
                   this.slots.add(i,this.slots.get(j)); 
                   remover(i+1);
                   this.slots.add(j,aux);
                   remover(j+1);
               }
           }
        }*/
   }
   
   public void ordenarItens(TipoOrdenacao ordem){
       for(int i = 0 ; i < this.slots.size() ; i++){
           for(int j = 0 ; j < this.slots.size() - 1; j++){
               if(this.slots.get(i).getQuantidade() < this.slots.get(j).getQuantidade()){
                   Item atual = this.slots.get(j);
                   Item proximo = this.slots.get(j + 1);
                   
                   boolean deveTrocar = ordem == TipoOrdenacao.ASC ? 
                   atual.getQuantidade() > proximo.getQuantidade() :
                   atual.getQuantidade() < proximo.getQuantidade();
                   
                   if( deveTrocar ){
                       Item itemTrocado = atual;
                       this.slots.set(j, proximo);
                       this.slots.set(j + 1, itemTrocado);
                   }
               }
           }
        }
   }
    
    
   
}
