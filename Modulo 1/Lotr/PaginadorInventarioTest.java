

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest {
   @Test
    public void limitarItens(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(5, "Espada");
        Item arco = new Item(10, "Arco");
        Item arc3 = new Item(8, "Arco");
        Item arc2 = new Item(7, "Arco");
        Item arc4 = new Item(11, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(arc2);
        itens.adicionar(arc3);
        itens.adicionar(arc4);
        PaginadorInventario paginador = new PaginadorInventario(itens);
        paginador.pular(2);
        assertEquals(arc2,paginador.limitar(1).get(0));
    }
}
