import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
    
    @Test
    public void adicionarUmItem(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        itens.adicionar(espada);
        assertEquals(espada,itens.obter(0));
    }
    
    @Test
    public void adicionarDoisItens(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        assertEquals(espada,itens.obter(0));
        assertEquals(arco,itens.obter(1));
    }
    
    @Test
    public void removerItem(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.remover(espada);
        assertEquals(arco,itens.obter(0));
    }
    
    @Test
    public void getDescricaoVariosItens(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        Item escudo = new Item(1, "Escudo");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(escudo);
        assertEquals("Espada,Arco,Escudo",itens.getDescricoes());
    }
    
    @Test
    public void getNenhumItem(){
        Inventario itens = new Inventario(11);
        assertEquals("",itens.getDescricoes());
    }
    
    @Test
    public void getItemMaiorQuantidade(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(3, "Arco");
        Item escudo = new Item(2, "Escudo");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(escudo);
        assertEquals(arco,itens.getItemComMaisQuantidade());
    }
    
    @Test
    public void getItemQuantidadesIguais(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(3, "Arco");
        Item escudo = new Item(3, "Escudo");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(escudo);
        assertEquals(arco,itens.getItemComMaisQuantidade());
    }
    
    
    @Test
    public void buscarPelaDescricao(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        assertEquals(espada,itens.busca("Espada"));
    }
    
    @Test
    public void buscarDescricaoIgual(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item espada1 = new Item(1, "Espada");
        itens.adicionar(espada);
        itens.adicionar(espada1);
        assertEquals(espada,itens.busca("Espada"));
    }
    
    @Test
    public void inverterOrdemArrayVazio(){
        Inventario itens = new Inventario(11);
        assertTrue(itens.inverter().isEmpty());
    }
    
    @Test
    public void inverterOrdemArrayUmItem(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        itens.adicionar(espada);
        assertEquals(itens.obter(0),itens.inverter().get(0));
        assertEquals(1, itens.inverter().size());
    }
    
    @Test
    public void inverterOrdemArray(){
        Inventario itens = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        itens.adicionar(espada);
        itens.adicionar(arco);
        assertEquals(arco,itens.inverter().get(0));
        assertEquals(espada,itens.inverter().get(1));
        assertEquals(2, itens.inverter().size());
    }
    
    @Test
    public void OrdenarPelaQuantidade(){
        Inventario itens = new Inventario(3);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(3, "Arco");
        Item lanca = new Item(2, "Lanca");
        Item escudo = new Item(2, "Escudo");
        itens.adicionar(espada);
        itens.adicionar(arco);
        itens.adicionar(lanca);
        itens.adicionar(escudo);
        itens.ordenarItens();
        assertEquals(espada,itens.obter(0));
        assertEquals(lanca,itens.obter(1));
        assertEquals(escudo,itens.obter(2));
        assertEquals(arco, itens.obter(3));
        assertEquals(4, itens.getSlots().size());
    }
    
    @Test
    public void adicionarUmItemEmCadaInventarioEUnir(){
        Inventario itens = new Inventario(11);
        Inventario itens2 = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        itens.adicionar(espada);
        itens2.adicionar(arco);
        itens.unir(itens2);
        assertEquals(espada,itens.obter(0));
        assertEquals(arco,itens.obter(1));
    }
    
    @Test
    public void adicionarUmItemEmCadaInventarioEUnirSeDiferente(){
        Inventario itens = new Inventario(11);
        Inventario itens2 = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        Item espadaDois = new Item(1, "Espada");
        itens.adicionar(espada);
        itens2.adicionar(arco);
        itens2.adicionar(espadaDois);
        itens.diferenciar(itens2);
        assertEquals(espada,itens.obter(0));
        assertEquals(arco,itens.obter(1));
        assertNull(itens.obter(2));
    }
    
    @Test
    public void SomarQuantidadeDeItensMesmaDescricao(){
        Inventario itens = new Inventario(11);
        Inventario itens2 = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        Item espadaDois = new Item(1, "Espada");
        itens.adicionar(espada);
        itens2.adicionar(arco);
        itens2.adicionar(espadaDois);
        itens.cruzar(itens2);
        assertEquals(2,itens.obter(0).getQuantidade());
    }
}
