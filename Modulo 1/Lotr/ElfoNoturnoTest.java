

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
  
    @Test
    public void atirarFlechaDiminuirFlechaAumentar3XpDiminuir15VidaElfoNoturno() {
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        assertEquals(3, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(85, novoElfo.getVida(), 1e-9);

    }
    
}
