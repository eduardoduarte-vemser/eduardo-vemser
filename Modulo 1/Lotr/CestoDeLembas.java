public class CestoDeLembas {
    
    private int lemba;
    
    {
    lemba = 0;
    }
    
    public CestoDeLembas (int lemba) {
        this.lemba = lemba;
    }
    
    public boolean podeDividirEmPares() {
        if((this.lemba - 2) >= 1 && (this.lemba - 2) <= 100) {
            if( (this.lemba - 2) % 2 == 0) {
                return true;
            }
        }
        return false;
    }
    
    
}
