

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest {
    
    @Test
    public void adicionarElfoAoExercito() {
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfos(novoElfo);
        assertEquals(novoElfo,exercito.getElfos().get(0));
    }
    
    @Test
    public void buscarElfoPorStatus() {
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        ElfoNoturno novoElfo2 = new ElfoNoturno("Legolas2");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfos(novoElfo);
        exercito.alistarElfos(novoElfo2);
        assertEquals(novoElfo,exercito.buscar(Status.RECEM_CRIADO).get(0));
    }
}
