import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());

    }
    
    @Test
    public void atirar2FlechasDiminuirFlechaAumentar2Xp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());

    }
    
    @Test
    public void atirar3FlechasDiminuirFlechaAumentar2Xp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());

    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());

    }
    
    @Test
    public void naoCriaElfoNaoIncrementa() {
        assertEquals(0, Elfo.getQtdElfos());

    }
    
    @Test
    public void Cria1ElfoIncrementaUmaVez() {
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());

    }
    
    @Test
    public void Cria2ElfosIncrementaDuasVez() {
        Elfo novoElfo = new Elfo("Legolas");
        Elfo novoElfo1 = new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());

    }
   
}
