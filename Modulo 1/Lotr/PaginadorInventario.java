import java.util.*;
public class PaginadorInventario {
    
    private Inventario inventario;
    private int marcador;
    
    {
        marcador = 0;
    }
    
    public PaginadorInventario( Inventario inventario ){
        this.inventario = inventario;
    }
    
    public void pular( int marcador ){
        this.marcador = marcador;
    }
    
    public ArrayList<Item> limitar( int limitador ){
        ArrayList<Item> itens = new ArrayList<>();
        int fim = this.marcador + limitador;
        for(int i = this.marcador ; i < fim && i < this.inventario.getSlots().size() ; i++) {
            itens.add( this.inventario.obter(i) );
        }
        return itens;
    }
}
