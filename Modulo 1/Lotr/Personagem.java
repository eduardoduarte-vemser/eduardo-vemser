public abstract class Personagem {
    
    protected String nome;
    protected Status status;
    protected Inventario itens;
    protected double vida,dano;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected boolean qtdAtaque;
    
    {
        status = Status.RECEM_CRIADO;
        itens = new Inventario(0);
        experiencia = 0;
        this.dano = 0.0;
        qtdExperienciaPorAtaque = 1;
        qtdAtaque = false;
    }
    
    public Personagem(String nome) {
        this.nome = nome;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome( String nome ){
        this.nome = nome;
    }
    
    public double getVida(){
        return this.vida;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario(){
        return this.itens;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    public void aumentarXp(){
        experiencia = experiencia + qtdExperienciaPorAtaque;
    }
    
    public void ganharItem( Item item ){
        this.itens.adicionar(item);
    }
    
    public void perderItem( Item item ) {
        this.itens.remover(item);
    }
    
    
    public void dano(){
        if(this.podeSofrerDano()){
            this.vida = this.vida >= this.dano ? this.vida - (this.dano) : 0.0;    
            if(this.vida == 0.0){
                this.status = Status.MORTO;
            }else{
                this.status = Status.SOFREU_DANO;
            }
        }
    }
    
    public abstract String imprimirResumo(); 
    
}
