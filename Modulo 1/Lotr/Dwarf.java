public class Dwarf extends Personagem{

    public Dwarf ( String nome ){
        super(nome);
        this.vida = 110.0;
        this.dano = 10.0;
        ganharItem(new Item(1, "Escudo"));
    }
    
   
    public void equiparEscudo(){
        this.dano = 5.0;
    }
    
    public String imprimirResumo(){
        return "Dwarf";
    }
    
}
