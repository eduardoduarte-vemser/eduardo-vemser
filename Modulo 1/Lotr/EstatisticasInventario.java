import java.util.*;

public class EstatisticasInventario {
    
    private Inventario itens;
    
    public EstatisticasInventario(Inventario itens){
        this.itens = itens;
    }
    
    public Inventario getInventario(){
        return this.itens;
    }
    
    public double calcularMedia(){
        if(this.itens.getSlots().isEmpty()){
            return Double.NaN;
        }
        double soma = 0.0;
        for(Item item : this.itens.getSlots()){
           soma += item.getQuantidade();
        }
        
        return soma / this.itens.getSlots().size();
    }
    
    public int[] ordenarArrayPorQuantidade(){
        int[] ordenar = new int[this.itens.getSlots().size()];
        int aux;
        for(int i = 0 ; i < this.itens.getSlots().size() ; i++){
           ordenar[i] = this.itens.obter(i).getQuantidade();
        }
        for(int i = 0 ; i < this.itens.getSlots().size() ; i++){
           for(int j = 0 ; j < this.itens.getSlots().size() ; j++){
               if(ordenar[i] < ordenar[j]){
                   aux = ordenar[i];
                   ordenar[i] = ordenar[j]; 
                   ordenar[j] = aux;
               }
           }
        }
        return ordenar;
    }
    
    public double calcularMediana(){
        if(this.itens.getSlots().isEmpty()){
            return Double.NaN;
        }
        this.itens.ordenarItens();
        int qtdItens = this.itens.getSlots().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.itens.obter(meio).getQuantidade();
        if(qtdItens % 2 == 1){
            return qtdMeio;
        }
        int qtdMeioMenosUm = this.itens.obter(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2.0;
    }
    
    
    public int qtdItensAcimaDaMedia(){
        int acimaDaMedia = 0;
        double media = this.calcularMedia();
        for( Item item : this.itens.getSlots()) {
            if(item.getQuantidade() > media){
               acimaDaMedia++;
            }
        }
        return acimaDaMedia;
    }
    
}
