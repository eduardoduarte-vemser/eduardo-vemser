
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CestoDeLembasTest {
    
    @Test
    public void retornarTrueQuandoParParaCadaPesronagem(){
        CestoDeLembas lembas = new CestoDeLembas(4);
        assertEquals(true,lembas.podeDividirEmPares());
    }
    
}
