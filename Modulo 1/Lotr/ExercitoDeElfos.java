
import java.util.*;

public class ExercitoDeElfos{
    
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    private ArrayList<Elfo> exercito = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap();
    
    public void alistarElfos( Elfo elfo ){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if( podeAlistar ) {
            this.exercito.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get( elfo.getStatus());
            if( elfoDoStatus == null ) {
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> buscar(Status status) {
        return this.porStatus.get(status);
    }
    
    
    public ArrayList<Elfo> getElfos(){
       
       return this.exercito;
       
    }
    

    
}
