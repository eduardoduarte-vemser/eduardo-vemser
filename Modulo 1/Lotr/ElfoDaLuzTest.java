
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    
    @Test
    public void atacarComEspadaAtaqueImparPerde21DeVida() {
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atacarComEspada(anao);
        assertEquals(79, novoElfo.getVida(),1e-9);

    }
    
    @Test
    public void atacarComEspadaAtaqueImparGanha10DeVida() {
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atacarComEspada(anao);
        novoElfo.atacarComEspada(anao); 
        assertEquals(89, novoElfo.getVida(),1e-9);

    }
    
    
}
