
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentar2Xp() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());

    }
    
    @Test
    public void atirar2FlechasDiminuirFlechaAumentar4Xp() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        assertEquals(4, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());

    }
    
    @Test
    public void atirar3FlechasDiminuirFlechaAumentar4Xp() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf anao = new Dwarf("Anao");
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        assertEquals(4, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());

    }
    
    @Test
    public void adicionarItemComDescricaoValida() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arcoDeVidro = new Item(1,"Arco de vidro");
        novoElfo.ganharItem(arcoDeVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2,"Flecha"),inventario.obter(0));
        assertEquals(new Item(1,"Arco"),inventario.obter(1));
        assertEquals(new Item(1,"Arco de vidro"),inventario.obter(2));

    }
    
    @Test
    public void adicionarItemComDescricaoInvalida() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arcoDeVidro = new Item(1,"Arco de madeira");
        novoElfo.ganharItem(arcoDeVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2,"Flecha"),inventario.obter(0));
        assertEquals(new Item(1,"Arco"),inventario.obter(1));
        assertNull(inventario.busca("Arco de madeira"));

    }
    
    @Test
    public void perderItemComDescricaoValida() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arcoDeVidro = new Item(1,"Arco de vidro");
        novoElfo.ganharItem(arcoDeVidro);
        novoElfo.perderItem(arcoDeVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2,"Flecha"),inventario.obter(0));
        assertEquals(new Item(1,"Arco"),inventario.obter(1));
        assertNull(inventario.busca("Arco de vidro"));

    }
    
    @Test
    public void perderItemComDescricaoInvalida() {
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arco = new Item(1,"Arco");
        Inventario inventario = novoElfo.getInventario();
        inventario.adicionar(arco);
        novoElfo.perderItem(arco);
        assertEquals(new Item(2,"Flecha"),inventario.obter(0));
        assertEquals(new Item(1,"Arco"),inventario.obter(1));
        assertEquals(arco.getDescricao(),inventario.obter(2).getDescricao());

    }
    
}
