import java.util.*;

public class ElfoDaLuz extends Elfo {
    
    private int qtdAtaque = 0;
    private double QTD_VIDA_GANHA = 10;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
        
    public ElfoDaLuz ( String nome ) {
        super(nome);
        this.dano = 21.0;
        super.ganharItem(new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public boolean devePerderVida() {
        return qtdAtaque % 2 == 1;
    }
    
    public void ganharVida() {
        this.vida += QTD_VIDA_GANHA;
    }
    
    public void atacarComEspada( Dwarf dwarf ) {
        //this.qtdAtaque = this.qtdAtaque + 1;
        dwarf.dano();
        this.aumentarXp();
        qtdAtaque++;
        if(devePerderVida()){
            this.dano();
        }else{
            this.ganharVida();
        }
    }
    
    @Override
    public void perderItem( Item item ) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            this.itens.remover(item);
        }
    }
    
    
    
    
}
