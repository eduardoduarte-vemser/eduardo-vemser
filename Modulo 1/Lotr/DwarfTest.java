import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    
    
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf anao = new Dwarf("Anao");
        assertEquals(110.0, anao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerde10deVida(){
        Dwarf anao = new Dwarf("Anao");
        anao.dano();
        assertEquals(100.0, anao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf anao = new Dwarf("Anao");
        for(int i = 0;i < 11; i++){
            anao.dano();
        }
        assertEquals(0.0, anao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerdeTodaVida12taques(){
        Dwarf anao = new Dwarf("Anao");
        assertEquals(Status.RECEM_CRIADO, anao.getStatus());
    }
    
    @Test
    public void dwarfNasceComStatus(){
        Dwarf anao = new Dwarf("Anao");
        assertEquals(Status.RECEM_CRIADO, anao.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf anao = new Dwarf("Anao");
        anao.dano();
        assertEquals(Status.SOFREU_DANO, anao.getStatus());
    }
    
    @Test
    public void dwarfPerdeTodaVidaStatusMudaParaMorto(){
        Dwarf anao = new Dwarf("Anao");
        for(int i = 0;i < 11; i++){
            anao.dano();
        }
        assertEquals(0.0, anao.getVida(), 1e-9);
        assertEquals(Status.MORTO, anao.getStatus());
    }
    
    @Test

    public void dwarfNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals("Escudo", dwarf.getInventario().busca("Escudo").getDescricao());
    }

    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.dano();
        assertEquals(105.0, dwarf.getVida(), 1e-9);

    }

    @Test

    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.dano();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
    
}
