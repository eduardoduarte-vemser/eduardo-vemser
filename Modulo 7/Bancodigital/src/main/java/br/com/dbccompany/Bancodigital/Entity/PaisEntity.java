package br.com.dbccompany.Bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAISES" )
@SequenceGenerator (allocationSize = 1, name = "PAISES_SEQ", sequenceName  = "PAISES_SEQ")

public class PaisEntity {

    @Id
    @GeneratedValue( generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "ID_PAIS")
    private Integer id_pais;

    private String nome;

    public Integer getId_pais() {
        return id_pais;
    }

    public void setId_pais(Integer id_pais) {
        this.id_pais = id_pais;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
