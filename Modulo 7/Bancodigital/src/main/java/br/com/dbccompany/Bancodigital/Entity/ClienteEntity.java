package br.com.dbccompany.Bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table( name = "CLIENTES" )
@SequenceGenerator (allocationSize = 1, name = "CLIENTE_SEQ", sequenceName  = "CLIENTE_SEQ")

public class ClienteEntity {

    @Id
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "ID_CLIENTE")
    private Integer id_cliente;

    private String nome;
    @Column ( name = "CPF")
    private Integer cpf;

    @ManyToMany (cascade = CascadeType.MERGE)
    @JoinTable( name = "CLIENTES_X_CIDADES",
            joinColumns = { @JoinColumn(name = "ID_CLIENTE") },
            inverseJoinColumns = {@JoinColumn(name = "ID_CIDADE")})
    private List<CidadeEntity> cidades = new ArrayList<>();

    @ManyToMany (cascade = CascadeType.MERGE)
    @JoinTable( name = "CLIENTES_X_CONTAS",
            joinColumns = { @JoinColumn(name = "ID_CLIENTE") },
            inverseJoinColumns = {@JoinColumn(name = "ID_CONTA")})
    private List<ContaEntity> contas = new ArrayList<>();

    
    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCpf() {
        return cpf;
    }

    public void setCpf(Integer cpf) {
        this.cpf = cpf;
    }
}
