package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.AgenciaEntity;
import br.com.dbccompany.Bancodigital.Entity.ClienteEntity;
import br.com.dbccompany.Bancodigital.Service.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/agencia" )
public class AgenciaController {

    @Autowired
    AgenciaService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<AgenciaEntity> todasAgencias(){
        return service.todasAgencias();
    }

    @PostMapping(value = "/nova")
    @ResponseBody
    public AgenciaEntity novaAgencia(@RequestBody AgenciaEntity agencia){
        return service.salvar(agencia);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AgenciaEntity editarAgencia(@PathVariable Integer id, @RequestBody AgenciaEntity agencia){
        return service.editar(agencia, id);
    }

    @GetMapping(value = "/buscarPorNome/{nome}")
    @ResponseBody
    public AgenciaEntity buscarAgenciaPorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

}