package br.com.dbccompany.Bancodigital.Service;


import br.com.dbccompany.Bancodigital.Entity.EstadoEntity;
import br.com.dbccompany.Bancodigital.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {

    @Autowired
    private EstadoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EstadoEntity salvar( EstadoEntity estado ){
        return repository.save(estado);
    }

    @Transactional( rollbackFor = Exception.class )
    public EstadoEntity editar( EstadoEntity estado, Integer id ){
        estado.setId_estado(id);
        return repository.save(estado);
    }

    public List<EstadoEntity> todosEstados(){

        return (List<EstadoEntity>) repository.findAll();

    }

    public EstadoEntity estadoEspecifico( Integer id ) {
        Optional<EstadoEntity> estado = repository.findById(id);
        return estado.get();
    }



}
