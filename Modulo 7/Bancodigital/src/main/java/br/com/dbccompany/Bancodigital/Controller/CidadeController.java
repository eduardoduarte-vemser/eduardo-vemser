package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.AgenciaEntity;
import br.com.dbccompany.Bancodigital.Entity.CidadeEntity;
import br.com.dbccompany.Bancodigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidade" )
public class CidadeController {

    @Autowired
    CidadeService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<CidadeEntity> todasCidades(){
        return service.todasCidades();
    }

    @PostMapping(value = "/nova")
    @ResponseBody
    public CidadeEntity novaCidade(@RequestBody CidadeEntity cidade){
        return service.salvar(cidade);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public CidadeEntity editarBanco(@PathVariable Integer id, @RequestBody CidadeEntity cidade){
        return service.editar(cidade, id);
    }

    @GetMapping(value = "/buscarPorNome/{nome}")
    @ResponseBody
    public CidadeEntity buscarCidadePorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }



}
