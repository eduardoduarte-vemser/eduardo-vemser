package br.com.dbccompany.Bancodigital.Repository;

import br.com.dbccompany.Bancodigital.Entity.MovimentacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovimentacaoRepository extends CrudRepository<MovimentacaoEntity, Integer> {

    MovimentacaoEntity findByTipoMovimentacao( String tipo );

}
