package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.BancoEntity;
import br.com.dbccompany.Bancodigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/banco" )
public class BancoController {

    @Autowired
    BancoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<BancoEntity> todosBancos(){
        return service.todosBancos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public BancoEntity novoBanco(@RequestBody BancoEntity banco){
        return service.salvar(banco);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public BancoEntity editarBanco(@PathVariable Integer id, @RequestBody BancoEntity banco){
        return service.editar(banco, id);
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public void excluirBanco(@PathVariable Integer id){
        service.excluirBanco(id);
    }


}
