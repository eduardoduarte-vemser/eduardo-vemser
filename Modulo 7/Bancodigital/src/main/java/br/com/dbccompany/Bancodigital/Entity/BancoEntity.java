package br.com.dbccompany.Bancodigital.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table( name = "BANCOS" )
@SequenceGenerator (allocationSize = 1, name = "BANCOS_SEQ", sequenceName  = "BANCOS_SEQ")

public class BancoEntity {

    @Id
    @GeneratedValue( generator = "BANCOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "CODIGO_BANCO")
    private Integer codigo_banco;

    private String nome;

    @OneToMany(
            mappedBy = "fk_codigo_banco",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    private List<AgenciaEntity> agencia = new ArrayList<>();

    public Integer getCodigo_banco() {
        return codigo_banco;
    }

    public void setCodigo_banco(Integer codigo_banco) {
        this.codigo_banco = codigo_banco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
