package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.PaisEntity;
import br.com.dbccompany.Bancodigital.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pais" )
public class PaisController {

    @Autowired
    PaisService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PaisEntity> todosBancos(){
        return service.todosPaises();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PaisEntity novoBanco(@RequestBody PaisEntity pais){
        return service.salvar(pais);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PaisEntity editarBanco(@PathVariable Integer id, @RequestBody PaisEntity pais){
        return service.editar(pais, id);
    }


}