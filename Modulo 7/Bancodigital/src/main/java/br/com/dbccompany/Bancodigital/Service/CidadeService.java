package br.com.dbccompany.Bancodigital.Service;

import br.com.dbccompany.Bancodigital.Entity.AgenciaEntity;
import br.com.dbccompany.Bancodigital.Entity.CidadeEntity;
import br.com.dbccompany.Bancodigital.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public CidadeEntity salvar(CidadeEntity cidade ){
        return repository.save(cidade);
    }

    @Transactional( rollbackFor = Exception.class )
    public CidadeEntity editar(CidadeEntity cidade, Integer id ){
        cidade.setId_cidade(id);
        return repository.save(cidade);
    }

    public List<CidadeEntity> todasCidades(){

        return (List<CidadeEntity>) repository.findAll();

    }

    public CidadeEntity cidadeEspecifica(Integer id ) {
        Optional<CidadeEntity> cidade = repository.findById(id);
        return cidade.get();
    }

    public CidadeEntity buscarPorNome(String nome ){
        return repository.findByNome(nome);
    }


}