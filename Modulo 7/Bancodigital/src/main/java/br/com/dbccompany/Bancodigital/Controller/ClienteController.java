package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.ClienteEntity;
import br.com.dbccompany.Bancodigital.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cliente" )
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClienteEntity> todosClientes(){
        return service.todosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteEntity novoCliente(@RequestBody ClienteEntity cliente){
        return service.salvar(cliente);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteEntity editarCliente(@PathVariable Integer id, @RequestBody ClienteEntity cliente){
        return service.editar(cliente, id);
    }

    @GetMapping(value = "/buscarPorCpf/{cpf}")
    @ResponseBody
    public ClienteEntity buscarClientePorCpf(@PathVariable String cpf){
        return service.buscarPorCpf(cpf);
    }

    @GetMapping(value = "/buscarPorNome/{nome}")
    @ResponseBody
    public ClienteEntity buscarClientePorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

}