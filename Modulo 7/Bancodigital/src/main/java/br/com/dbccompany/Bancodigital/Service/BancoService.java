package br.com.dbccompany.Bancodigital.Service;


import br.com.dbccompany.Bancodigital.Entity.BancoEntity;
import br.com.dbccompany.Bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public BancoEntity salvar( BancoEntity banco ){
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public BancoEntity editar( BancoEntity banco, Integer id ){
        banco.setCodigo_banco(id);
        return repository.save(banco);
    }

    public List<BancoEntity> todosBancos(){

        return (List<BancoEntity>) repository.findAll();

    }

    public BancoEntity bancoEspecifico( Integer id ) {
        Optional<BancoEntity> banco = repository.findById(id);
        return banco.get();
    }

    public void excluirBanco( Integer id ){
        repository.deleteById(id);
    }



}
