package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.ContaEntity;
import br.com.dbccompany.Bancodigital.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/conta" )
public class ContaController {

    @Autowired
    ContaService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<ContaEntity> todasContas(){
        return service.todasContas();
    }

    @PostMapping(value = "/nova")
    @ResponseBody
    public ContaEntity novaConta(@RequestBody ContaEntity conta){
        return service.salvar(conta);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContaEntity editarConta(@PathVariable Integer id, @RequestBody ContaEntity conta){
        return service.editar(conta, id);
    }

    @GetMapping(value = "/buscarPorNumero/{numero}")
    @ResponseBody
    public ContaEntity buscarClientePorNumero(@PathVariable Integer numero){
        return service.buscarPorNumero(numero);
    }


}
