package br.com.dbccompany.Bancodigital.Service;

import br.com.dbccompany.Bancodigital.Entity.AgenciaEntity;
import br.com.dbccompany.Bancodigital.Entity.ClienteEntity;
import br.com.dbccompany.Bancodigital.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AgenciaService {

    @Autowired
    private AgenciaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public AgenciaEntity salvar(AgenciaEntity agencia ){
        return repository.save(agencia);
    }

    @Transactional( rollbackFor = Exception.class )
    public AgenciaEntity editar(AgenciaEntity agencia, Integer id ){
        agencia.setCodigo_agencia(id);
        return repository.save(agencia);
    }

    public List<AgenciaEntity> todasAgencias(){

        return (List<AgenciaEntity>) repository.findAll();

    }

    public AgenciaEntity buscarPorNome(String nome ){
        return repository.findByNome(nome);
    }


    public AgenciaEntity agenciaEspecifica(Integer id ) {
        Optional<AgenciaEntity> cidade = repository.findById(id);
        return cidade.get();
    }

}
