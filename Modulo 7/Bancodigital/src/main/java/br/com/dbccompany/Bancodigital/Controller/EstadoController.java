package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.EstadoEntity;
import br.com.dbccompany.Bancodigital.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/estado" )
public class EstadoController {

    @Autowired
    EstadoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EstadoEntity> todosEstados(){
        return service.todosEstados();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EstadoEntity novoEstado(@RequestBody EstadoEntity estado){
        return service.salvar(estado);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EstadoEntity editarEstado(@PathVariable Integer id, @RequestBody EstadoEntity estado){
        return service.editar(estado, id);
    }


}
