package br.com.dbccompany.Bancodigital.Entity;

import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.OnDelete;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table( name = "CONTAS" )
@SequenceGenerator (allocationSize = 1, name = "CONTAS_SEQ", sequenceName  = "CONTAS_SEQ")


public class ContaEntity {

    @Id
    @GeneratedValue( generator = "CONTAS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "ID_CONTA")
    private Integer idConta;

    @Enumerated( EnumType.STRING )
    private TipoConta tipoConta;

    @ManyToOne (cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_CODIGO_AGENCIA")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private AgenciaEntity fk_codigo_agencia;

    @ManyToMany (cascade = CascadeType.MERGE)
    @JoinTable( name = "CLIENTES_X_CONTAS",
            joinColumns = { @JoinColumn(name = "ID_CONTA") },
            inverseJoinColumns = {@JoinColumn(name = "ID_CLIENTE")})
    private List<ClienteEntity> clientes = new ArrayList<>();

    private Integer numero;

    private Double saldo;

    public Integer getIdConta() {
        return idConta;
    }

    public void setIdConta(Integer idConta) {
        this.idConta = idConta;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    public AgenciaEntity getFk_codigo_agencia() {
        return fk_codigo_agencia;
    }

    public void setFk_codigo_agencia(AgenciaEntity fk_codigo_agencia) {
        this.fk_codigo_agencia = fk_codigo_agencia;
    }

    public List<ClienteEntity> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteEntity> clientes) {
        this.clientes = clientes;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
}
