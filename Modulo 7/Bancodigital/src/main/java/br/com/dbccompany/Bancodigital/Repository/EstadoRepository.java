package br.com.dbccompany.Bancodigital.Repository;

import br.com.dbccompany.Bancodigital.Entity.EstadoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoRepository extends CrudRepository<EstadoEntity, Integer> {

    EstadoEntity findByNome( String nome );
//    List<EstadoEntity> findAll();

}
