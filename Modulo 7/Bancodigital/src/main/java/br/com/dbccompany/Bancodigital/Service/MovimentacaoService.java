package br.com.dbccompany.Bancodigital.Service;

import br.com.dbccompany.Bancodigital.Entity.MovimentacaoEntity;
import br.com.dbccompany.Bancodigital.Repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MovimentacaoService {

    @Autowired
    private MovimentacaoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public MovimentacaoEntity salvar(MovimentacaoEntity movimentacao ){
        return repository.save(movimentacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public MovimentacaoEntity editar(MovimentacaoEntity movimentacao, Integer id ){
        movimentacao.setIdMovimentacao(id);
        return repository.save(movimentacao);
    }

    public List<MovimentacaoEntity> todasMovimentacoes(){

        return (List<MovimentacaoEntity>) repository.findAll();

    }

    public MovimentacaoEntity movimentacaoEspecifica(Integer id ) {
        Optional<MovimentacaoEntity> movimentacao = repository.findById(id);
        return movimentacao.get();
    }

}
