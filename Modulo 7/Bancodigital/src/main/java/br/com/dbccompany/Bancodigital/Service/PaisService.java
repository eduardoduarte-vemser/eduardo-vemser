package br.com.dbccompany.Bancodigital.Service;


import br.com.dbccompany.Bancodigital.Entity.PaisEntity;
import br.com.dbccompany.Bancodigital.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PaisService {

    @Autowired
    private PaisRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PaisEntity salvar( PaisEntity pais ){
        return repository.save(pais);
    }

    @Transactional( rollbackFor = Exception.class )
    public PaisEntity editar( PaisEntity pais, Integer id ){
        pais.setId_pais(id);
        return repository.save(pais);
    }

    public List<PaisEntity> todosPaises(){

        return (List<PaisEntity>) repository.findAll();

    }

    public PaisEntity paisEspecifico( Integer id ) {
        Optional<PaisEntity> pais = repository.findById(id);
        return pais.get();
    }



}
