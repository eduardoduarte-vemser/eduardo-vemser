package br.com.dbccompany.Bancodigital.Controller;


import br.com.dbccompany.Bancodigital.Entity.MovimentacaoEntity;
import br.com.dbccompany.Bancodigital.Service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/movimentacao" )
public class MovimentacaoController {

    @Autowired
    MovimentacaoService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<MovimentacaoEntity> todasMovimentacoes(){
        return service.todasMovimentacoes();
    }

    @PostMapping(value = "/nova")
    @ResponseBody
    public MovimentacaoEntity novaMovimentacao(@RequestBody MovimentacaoEntity movimentacao){
        return service.salvar(movimentacao);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public MovimentacaoEntity editarMovimentacao(@PathVariable Integer id, @RequestBody MovimentacaoEntity movimentacao){
        return service.editar(movimentacao, id);
    }


}