package br.com.dbccompany.Bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "MOVIMENTACOES" )
@SequenceGenerator (allocationSize = 1, name = "MOVIMENTACOES_SEQ", sequenceName  = "MOVIMENTACOES_SEQ")

public class MovimentacaoEntity {

    @Id
    @GeneratedValue( generator = "MOVIMENTACOES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "ID_MOVIMENTACAO")
    private Integer idMovimentacao;

    @Enumerated( EnumType.STRING )
    private TipoMovimentacao tipoMovimentacao;

    public Integer getIdMovimentacao() {
        return idMovimentacao;
    }

    public void setIdMovimentacao(Integer idMovimentacao) {
        this.idMovimentacao = idMovimentacao;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }
}
