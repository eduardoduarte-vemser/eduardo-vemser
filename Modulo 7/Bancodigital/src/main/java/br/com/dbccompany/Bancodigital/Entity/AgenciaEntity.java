package br.com.dbccompany.Bancodigital.Entity;

import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "AGENCIAS" )
@SequenceGenerator (allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName  = "AGENCIAS_SEQ")

public class AgenciaEntity {

    @Id
    @GeneratedValue( generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "CODIGO_AGENCIA")
    private Integer codigo_agencia;

    private String nome;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_CODIGO_BANCO")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private BancoEntity fk_codigo_banco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_ID_CIDADE")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private CidadeEntity fk_id_cidade;

    @OneToMany(
            mappedBy = "fk_codigo_agencia",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    private List<ContaEntity> conta = new ArrayList<>();

    public Integer getCodigo_agencia() {
        return codigo_agencia;
    }

    public void setCodigo_agencia(Integer codigo_agencia) {
        this.codigo_agencia = codigo_agencia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BancoEntity getFk_codigo_banco() {
        return fk_codigo_banco;
    }

    public void setFk_codigo_banco(BancoEntity fk_codigo_banco) {
        this.fk_codigo_banco = fk_codigo_banco;
    }

    public CidadeEntity getFk_id_cidade() {
        return fk_id_cidade;
    }

    public void setFk_id_cidade(CidadeEntity fk_id_cidade) {
        this.fk_id_cidade = fk_id_cidade;
    }
}
