package br.com.dbccompany.Bancodigital.Repository;

import br.com.dbccompany.Bancodigital.Entity.ClienteEntity;
import br.com.dbccompany.Bancodigital.Entity.ContaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaRepository extends CrudRepository<ContaEntity, Integer> {

    ContaEntity findByNumero(Integer numero );

}
