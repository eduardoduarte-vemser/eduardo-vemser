package br.com.dbccompany.Bancodigital.Repository;

import br.com.dbccompany.Bancodigital.Entity.BancoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BancoRepository extends CrudRepository<BancoEntity, Integer> {

    BancoEntity findByNome( String nome );
//    List<BancoEntity> findAll();

}
