package br.com.dbccompany.Bancodigital.Service;


import br.com.dbccompany.Bancodigital.Entity.ClienteEntity;
import br.com.dbccompany.Bancodigital.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity salvar(ClienteEntity cliente ){
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity editar( ClienteEntity cliente, Integer id ){
        cliente.setId_cliente(id);
        return repository.save(cliente);
    }

    public List<ClienteEntity> todosClientes(){

        return (List<ClienteEntity>) repository.findAll();

    }


    public ClienteEntity buscarPorCpf( String cpf ){
        return repository.findByCpf(cpf);
    }
    public ClienteEntity buscarPorNome( String nome ){
        return repository.findByNome(nome);
    }

    public ClienteEntity clienteEspecifico( Integer id ) {
        Optional<ClienteEntity> cliente = repository.findById(id);
        return cliente.get();
    }



}
