package br.com.dbccompany.Bancodigital.Entity;

import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;

@Entity
@Table( name = "ESTADOS" )
@SequenceGenerator (allocationSize = 1, name = "ESTADOS_SEQ", sequenceName  = "ESTADOS_SEQ")

public class EstadoEntity {

    @Id
    @GeneratedValue( generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "ID_ESTADO")
    private Integer id_estado;

    private String nome;


    @ManyToOne (cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_ID_PAIS")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PaisEntity fk_id_pais;

    public Integer getId_estado() {
        return id_estado;
    }

    public void setId_estado(Integer id_estado) {
        this.id_estado = id_estado;
    }

    public String getEstado() {
        return nome;
    }

    public void setEstado(String nome) {
        this.nome = nome;
    }

    public PaisEntity getFk_id_pais() {
        return fk_id_pais;
    }

    public void setFk_id_pais(PaisEntity fk_id_pais) {
        this.fk_id_pais = fk_id_pais;
    }
}
