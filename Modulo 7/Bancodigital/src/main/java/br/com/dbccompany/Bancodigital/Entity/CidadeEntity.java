package br.com.dbccompany.Bancodigital.Entity;

import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.OnDelete;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table( name = "CIDADES" )
@SequenceGenerator (allocationSize = 1, name = "CIDADES_SEQ", sequenceName  = "CIDADES_SEQ")


public class CidadeEntity {

    @Id
    @GeneratedValue( generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column ( name = "ID_CIDADE")
    private Integer id_cidade;

    @OneToMany(
            mappedBy = "fk_id_cidade",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    private List<AgenciaEntity> agencia = new ArrayList<>();

    private String nome;
    @ManyToMany( cascade = CascadeType.MERGE)
    @JoinTable( name = "CLIENTES_X_CIDADES",
            joinColumns = { @JoinColumn(name = "ID_CIDADE") },
            inverseJoinColumns = {@JoinColumn(name = "ID_CLIENTE")})
    private List<ClienteEntity> clientes = new ArrayList<>();

    @ManyToOne (cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_ID_ESTADO")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private EstadoEntity fk_id_estado;


    public Integer getId_cidade() {
        return id_cidade;
    }

    public void setId_cidade(Integer id_cidade) {
        this.id_cidade = id_cidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EstadoEntity getFk_id_estado() {
        return fk_id_estado;
    }

    public void setFk_id_estado(EstadoEntity fk_id_estado) {
        this.fk_id_estado = fk_id_estado;
    }
}
