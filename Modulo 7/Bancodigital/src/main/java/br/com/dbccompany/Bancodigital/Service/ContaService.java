package br.com.dbccompany.Bancodigital.Service;

import br.com.dbccompany.Bancodigital.Entity.ClienteEntity;
import br.com.dbccompany.Bancodigital.Entity.ContaEntity;
import br.com.dbccompany.Bancodigital.Repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContaService {

    @Autowired
    private ContaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ContaEntity salvar(ContaEntity conta ){
        return repository.save(conta);
    }

    @Transactional( rollbackFor = Exception.class )
    public ContaEntity editar(ContaEntity conta, Integer id ){
        conta.setIdConta(id);
        return repository.save(conta);
    }

    public List<ContaEntity> todasContas(){

        return (List<ContaEntity>) repository.findAll();

    }

    public ContaEntity contaEspecifica(Integer id ) {
        Optional<ContaEntity> conta = repository.findById(id);
        return conta.get();
    }

    public ContaEntity buscarPorNumero(Integer numero ){
        return repository.findByNumero(numero);
    }

}
