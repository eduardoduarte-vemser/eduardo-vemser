package com.coworking.coworking.Entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Entity
@Table(name = "USUARIOS")
@SequenceGenerator (allocationSize = 1, name = "USUARIOS_SEQ", sequenceName  = "USUARIOS_SEQ")

public class Usuario {

    @Id
    @GeneratedValue( generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    @Size(min = 6)
    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {

        try{
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(senha.getBytes());
            byte[] digest = md5.digest();
            String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
            this.senha = myHash;
        }catch (NoSuchAlgorithmException e) {
            System.err.println(e.getMessage());
        }

    }
}
