package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {


}
