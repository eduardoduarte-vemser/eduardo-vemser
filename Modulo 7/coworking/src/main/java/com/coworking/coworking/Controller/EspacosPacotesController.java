package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.EspacosPacotes;
import com.coworking.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<EspacosPacotes> buscarTodos() {
        return service.buscarTodos();
    }

    @PostMapping("/novo")
    @ResponseBody
    public EspacosPacotes salvar (@RequestBody EspacosPacotes espacosPacotes) {
        return service.salvar(espacosPacotes);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public EspacosPacotes editar ( @RequestBody EspacosPacotes espacosPacotes, @PathVariable Integer id ) {
        return service.editar(espacosPacotes,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public boolean deletar (Integer id) {
        service.deletar(id);
        return true;
    }
}
