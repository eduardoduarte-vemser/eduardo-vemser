package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Pagamentos;
import com.coworking.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Pagamentos> buscarTodos() {
        return service.buscarTodos();
    }

    @PostMapping("/novo")
    @ResponseBody
    public Pagamentos salvar(@RequestBody Pagamentos pagamentos) {
        return service.salvar(pagamentos);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Pagamentos editar(@RequestBody Pagamentos pagamentos, @PathVariable Integer id) {
        return service.editar(pagamentos,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public boolean deletar( Integer id ){
        service.deletar(id);
        return true;
    }

}
