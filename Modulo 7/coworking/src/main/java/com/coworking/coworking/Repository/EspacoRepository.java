package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacoRepository  extends CrudRepository<Espaco, Integer> {
}
