package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {
}
