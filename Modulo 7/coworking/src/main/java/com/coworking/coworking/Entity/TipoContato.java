package com.coworking.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TIPO_CONTATO")
@SequenceGenerator(allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName  = "TIPO_CONTATO_SEQ")
public class TipoContato {

    @Id
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @OneToMany(
            mappedBy = "tipoContato",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    List<Contato> contato = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContato() {
        return contato;
    }

    public void setContato(List<Contato> contato) {

        this.contato = contato;
    }
}
