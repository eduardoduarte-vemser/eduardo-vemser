package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.ClientesPacotes;
import com.coworking.coworking.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesPacotesService {

    @Autowired
    ClientesPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes ) {
        return repository.save(clientesPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes editar(ClientesPacotes clientesPacotes, Integer id) {
        clientesPacotes.setId(id);
        return repository.save(clientesPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id) {
        repository.deleteById(id);
    }

    public List<ClientesPacotes> buscarTodos() {
        return (List<ClientesPacotes>) repository.findAll();
    }
}
