package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.ClientesPacotes;
import com.coworking.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService service;

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesPacotes novoClientesPacotes(@RequestBody ClientesPacotes clientesPacotes) {
        return service.salvar(clientesPacotes);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesPacotes editar(@PathVariable Integer id, @RequestBody ClientesPacotes clientesPacotes) {
        return service.editar(clientesPacotes,id);
    }

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotess(){
        return service.buscarTodos();
    }

    @DeleteMapping( value = "/deletar/{id}")
    @ResponseBody
    public void deletar(@PathVariable Integer id) {
        service.deletar(id);
    }
}
