package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    Contato findByValor(String valor);

}
