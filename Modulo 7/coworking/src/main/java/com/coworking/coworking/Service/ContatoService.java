package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Contato;
import com.coworking.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Contato salvar( Contato contato ) {
        return repository.save(contato);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contato editar(Contato contato, Integer id) {
        contato.setId(id);
        return repository.save(contato);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id) {
        repository.deleteById(id);
    }

    public List<Contato> buscarTodos() {
        return (List<Contato>) repository.findAll();
    }

    public Contato buscarPorNome( String nome ) {
        return repository.findByValor(nome);
    }


}
