package com.coworking.coworking.Entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_PACOTES")
@SequenceGenerator(allocationSize = 1,name = "ESPACOS_PACOTES_SEQ",sequenceName =  "ESPACOS_PACOTES_SEQ")
public class EspacosPacotes {

    @Id
    @GeneratedValue(generator =  "ESPACOS_PACOTES_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_ESPACO",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Espaco idEspaco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_PACOTE",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Pacote idPacote;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private  Integer quantidade;

    @Column(nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Pacote getIdPacote() {
        return idPacote;
    }

    public void setIdPacote(Pacote idPacote) {
        this.idPacote = idPacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
