package com.coworking.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.OnDelete;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SALDO_CLIENTES")
public class SaldoClientes {

    @EmbeddedId
    private SaldoClientesId id;

    @ManyToOne
    @MapsId("ID_CLIENTE")
    @JoinColumn( name = "ID_CLIENTE", nullable = false)
    private Cliente cliente;

    @ManyToOne
    @MapsId("ID_ESPACO")
    @JoinColumn( name = "ID_ESPACO", nullable = false)
    private Espaco espaco;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm" )
    @Column( name = "VENCIMENTO", nullable = false )
    private Date vencimento;

    public SaldoClientesId getId() {
        return id;
    }

    public void setId(SaldoClientesId id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
