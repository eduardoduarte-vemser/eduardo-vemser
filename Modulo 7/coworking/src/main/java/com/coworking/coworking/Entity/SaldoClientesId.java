package com.coworking.coworking.Entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SaldoClientesId implements Serializable {

    @Column( name = "ID_CLIENTE" )
    private Integer idCliente;

    @Column( name = "ID_ESPACO" )
    private Integer idEspaco;

    public SaldoClientesId(){

    }

    public SaldoClientesId(Integer idCliente, Integer idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Integer idEspaco) {
        this.idEspaco = idEspaco;
    }
}
