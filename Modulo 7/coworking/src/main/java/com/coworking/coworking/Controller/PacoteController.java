package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Pacote;
import com.coworking.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

    @Autowired
    PacoteService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Pacote> buscarTodos() {
        return service.buscarTodos();
    }

    @PostMapping("/novo")
    @ResponseBody
    public Pacote salvar (@RequestBody Pacote pacote) {
        return service.salvar(pacote);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Pacote editar ( @RequestBody Pacote pacote, @PathVariable Integer id ) {
        return service.editar(pacote,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public boolean deletar (Integer id) {
        service.deletar(id);
        return true;
    }


}
