package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Cliente;
import com.coworking.coworking.Entity.Contato;
import com.coworking.coworking.Entity.TipoContato;
import com.coworking.coworking.Service.ClienteService;
import com.coworking.coworking.Service.ContatoService;
import com.coworking.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    ClienteService service;

    @PostMapping( value = "/novo")
    @ResponseBody
    public Cliente novoCliente(@RequestBody Cliente cliente) {
            return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Cliente editar(@PathVariable Integer id, @RequestBody Cliente cliente) {
        return service.editar(cliente,id);
    }

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Cliente> todosClientes(){
        return service.todosClientes();
    }

    @GetMapping( value = "/buscarPorNome/{nome}")
    @ResponseBody
    public Cliente buscarPorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

    @GetMapping( value = "/buscarPorCpf/{cpf}")
    @ResponseBody
    public Cliente buscarPorCpf(@PathVariable String cpf){
        return service.buscarPorCpf(cpf);
    }

    @DeleteMapping( value = "/deletar/{id}")
    @ResponseBody
    public void deletar(@PathVariable Integer id) {
        service.deletar(id);
    }

}
