package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.SaldoClientes;
import com.coworking.coworking.Entity.SaldoClientesId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClientesRepository extends CrudRepository<SaldoClientes, SaldoClientesId> {

}
