package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Contratacao;
import com.coworking.coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    ContratacaoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Contratacao salvar( Contratacao contratacao ) {
        return repository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editar( Contratacao contratacao, Integer id ) {
        contratacao.setId(id);
        return repository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

    public List<Contratacao> buscarTodos() {
        return (List<Contratacao>) repository.findAll();
    }

    public Contratacao buscarPorId( Integer id ) {
        Optional<Contratacao> contratacao = repository.findById(id);
        return contratacao.get();
    }

}
