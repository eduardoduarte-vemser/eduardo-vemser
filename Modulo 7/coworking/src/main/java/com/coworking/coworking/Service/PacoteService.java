package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Pacote;
import com.coworking.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PacoteService {

    @Autowired
    PacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Pacote salvar( Pacote pacote ) {
        return repository.save(pacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacote editar( Pacote pacote, Integer id ) {
        pacote.setId(id);
        return repository.save(pacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

    public List<Pacote> buscarTodos() {
        return (List<Pacote>) repository.findAll();
    }

}
