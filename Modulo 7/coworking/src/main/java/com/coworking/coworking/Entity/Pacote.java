package com.coworking.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PACOTES")
@SequenceGenerator(allocationSize = 1,name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
public class Pacote {

    @Id
    @GeneratedValue(generator = "PACOTES_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private Double valor;

    @OneToMany(
            mappedBy = "idPacote",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(
            mappedBy = "idPacote",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        String valorString = "R$ " + valor.toString();
        return valorString;
    }

    public void setValor(String valor) {
        valor = valor.replace(" ","");
        valor = valor.replace(",",".");
        valor = valor.replace("R$","");
        this.valor = Double.parseDouble(valor);
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
