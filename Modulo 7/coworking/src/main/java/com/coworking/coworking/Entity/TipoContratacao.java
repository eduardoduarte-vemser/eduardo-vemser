package com.coworking.coworking.Entity;

public enum TipoContratacao {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
