package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Acessos;
import com.coworking.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    AcessosRepository repository;

    public String strZero(Integer valor){
        String valorString;
        if( valor < 10 ) {
            valorString = "0" + valor.toString();
            return valorString;
        }
        return valor.toString();
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos salvar( Acessos acesso ) {
        acesso.setEntrada(true);
        if(acesso.getData() == null){
            inserirData(acesso);
        }
        return repository.save(acesso);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Acessos acesso, Integer id) {
        acesso.setId(id);
        return repository.save(acesso);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

    public List<Acessos> buscarTodos() {
        return (List<Acessos>) repository.findAll();
    }

    public Acessos buscarPorId( Integer id ) {
        Optional<Acessos> acesso = repository.findById(id);
        return acesso.get();
    }

    public void inserirData(Acessos acesso){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date hora = new Date();
        String horaFormatada = sdf.format(hora);

        acesso.setData(horaFormatada);
    }

}
