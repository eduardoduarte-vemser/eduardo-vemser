package com.coworking.coworking.Entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "CONTRATACAO")
@SequenceGenerator(allocationSize = 1,name = "CONTRATACAO_SEQ",sequenceName = "CONTRATACAO_SEQ")
public class Contratacao {

    @Id
    @GeneratedValue(generator = "CONTRATACAO_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_ESPACOS",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Espaco idEspaco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Cliente idCliente;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private Double quantidade;

    private Double desconto;

    @Column(nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espaco getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Espaco idEspaco) {
        this.idEspaco = idEspaco;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
