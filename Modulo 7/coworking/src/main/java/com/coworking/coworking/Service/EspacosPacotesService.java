package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.EspacosPacotes;
import com.coworking.coworking.Repository.EspacosPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosPacotesService {

    @Autowired
    EspacosPacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacoPacotes ) {
        return repository.save(espacoPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes editar( EspacosPacotes espacoPacotes, Integer id ) {
        espacoPacotes.setId(id);
        return repository.save(espacoPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

    public List<EspacosPacotes> buscarTodos() {
        return (List<EspacosPacotes>) repository.findAll();
    }
}
