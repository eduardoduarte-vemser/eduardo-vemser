package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosPacoteRepository extends CrudRepository<EspacosPacotes, Integer> {
}
