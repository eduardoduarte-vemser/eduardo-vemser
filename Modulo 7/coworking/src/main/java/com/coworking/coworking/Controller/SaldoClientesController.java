package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.SaldoClientes;
import com.coworking.coworking.Entity.SaldoClientesId;
import com.coworking.coworking.Service.SaldoClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldoClientes")
public class SaldoClientesController {

    @Autowired
    SaldoClientesService service;

    @PostMapping( value = "/novo")
    @ResponseBody
    public SaldoClientes novoSaldoClientes(@RequestBody SaldoClientes saldoClientes) {
        return service.salvar(saldoClientes);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public SaldoClientes editar(@PathVariable SaldoClientesId id, @RequestBody SaldoClientes saldoClientes) {
        return service.editar(saldoClientes,id);
    }

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<SaldoClientes> todosSaldoClientess(){
        return service.buscarTodos();
    }

    @DeleteMapping( value = "/deletar/{id}")
    @ResponseBody
    public void deletar(@PathVariable SaldoClientesId id) {
        service.deletar(id);
    }
}
