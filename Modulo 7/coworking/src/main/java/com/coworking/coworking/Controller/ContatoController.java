package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Contato;
import com.coworking.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Contato> buscarTodos() {
        return service.buscarTodos();
    }

    @GetMapping("/buscarPorNome/{valor}")
    @ResponseBody
    public Contato buscarPorNome(@PathVariable String valor) {
        return service.buscarPorNome(valor);
    }

    @PostMapping("/novo")
    @ResponseBody
    public Contato novoContato(@RequestBody Contato contato) {
        return service.salvar(contato);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Contato editar(@PathVariable Integer id, @RequestBody Contato contato) {
        return service.editar(contato,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public void deletar(@PathVariable Integer id ){
        service.deletar(id);
    }

}
