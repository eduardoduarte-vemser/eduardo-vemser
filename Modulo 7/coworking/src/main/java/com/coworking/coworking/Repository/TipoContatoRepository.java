package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer> {

    TipoContato findByNome( String nome );
}
