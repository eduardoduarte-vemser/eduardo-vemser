package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;

public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer> {


}
