package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Espaco;
import com.coworking.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoService {

    @Autowired
    EspacoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espaco salvar( Espaco espaco ) {
        return repository.save(espaco);
    }

    @Transactional(rollbackFor = Exception.class)
    public Espaco editar( Espaco espaco, Integer id ) {
        espaco.setId(id);
        return repository.save(espaco);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

    public List<Espaco> buscarTodos() {
        return (List<Espaco>) repository.findAll();
    }

}
