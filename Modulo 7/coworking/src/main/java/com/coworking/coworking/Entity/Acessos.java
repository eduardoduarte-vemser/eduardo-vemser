package com.coworking.coworking.Entity;


import javax.persistence.*;

@Entity
@Table(name = "ACESSOS")
@SequenceGenerator(allocationSize = 1,name = "ACESSOS_SEQ",sequenceName = "ACESSOS_SEQ")
public class Acessos {

    @Id
    @GeneratedValue(generator = "ACESSOS_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumns(value = {
            @JoinColumn(name = "ID_CLIENTE_SALDO_CLIENTE", referencedColumnName = "ID_CLIENTE"),
            @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE", referencedColumnName = "ID_ESPACO")
    })
    private SaldoClientes saldoClientes;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    private Integer quantidade;

    @Column(name = "IS_ENTRADA")
    private boolean isEntrada;

    private String data;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
