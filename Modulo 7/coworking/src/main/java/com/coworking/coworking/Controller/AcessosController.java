package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Acessos;
import com.coworking.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Acessos> buscarTodos(){
        return service.buscarTodos();
    }

    @GetMapping("/buscarId/{id}")
    @ResponseBody
    public Acessos buscarId(@PathVariable Integer id) {
        return service.buscarPorId(id);
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public Acessos salvar(@RequestBody Acessos acesso) {
        return service.salvar(acesso);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Acessos editar(@PathVariable Integer id, @RequestBody Acessos acesso) {
        return service.editar(acesso,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public boolean deletar( Integer id ) {
        service.deletar(id);
        return true;
    }
}
