package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<Pacote, Integer> {

}
