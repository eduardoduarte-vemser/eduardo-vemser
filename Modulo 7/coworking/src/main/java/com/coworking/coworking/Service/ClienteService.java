package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Cliente;
import com.coworking.coworking.Entity.Contato;
import com.coworking.coworking.Entity.TipoContato;
import com.coworking.coworking.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Transactional(rollbackFor = Exception.class)
    public Cliente salvar( Cliente cliente ) {
        boolean contEmail =false, contTel = false;
        List<Contato> contato = cliente.getContato();
        for (int i = 1; i <= contato.size() ; i ++){
            TipoContato tipoContato = tipoContatoService.buscarPorId(i);
            if(tipoContato.getNome().equals("Email")){
                contEmail = true;
            }
            if(tipoContato.getNome().equals("Telefone")){
                contTel = true;
            }
        }
        if(contEmail && contTel ) {
            return repository.save(cliente);
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente editar( Cliente cliente, Integer id ) {
        cliente.setId(id);
        return repository.save(cliente);
    }

    public List<Cliente> todosClientes() {
        return (List<Cliente>) repository.findAll();
    }

    public Cliente buscarPorNome( String nome ) {
        return repository.findByNome(nome);
    }

    public Cliente buscarPorCpf( String cpf ) {
        return repository.findByCpf(cpf);
    }

    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

}
