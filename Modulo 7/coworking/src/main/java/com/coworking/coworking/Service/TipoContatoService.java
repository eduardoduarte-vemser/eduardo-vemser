package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.TipoContato;
import com.coworking.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    TipoContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato contato) {
        return repository.save(contato);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContato editar(TipoContato tipoContato, Integer id) {
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }

    public List<TipoContato> todosTiposContato() {
        return (List<TipoContato>) repository.findAll();
    }

    public TipoContato buscarPorNome( String nome ) {
        return repository.findByNome( nome );
    }

    public String buscarNome(String nome){
        TipoContato contato = buscarPorNome(nome);
        return contato.getNome();
    }

    public TipoContato buscarPorId(Integer id) {
        Optional<TipoContato> tipoContato = repository.findById(id);
        return tipoContato.get();
    }

    public void deletar( Integer id ) {
        repository.deleteById(id);
    }


}
