package com.coworking.coworking.Entity;

import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ESPACOS")
@SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName  = "ESPACOS_SEQ")
public class Espaco {

    @Id
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(name = "QTD_PESSOAS", nullable = false)
    private Integer qtdPessoas;

    @Column(nullable = false)
    private Double valor;

    @OneToMany(
            mappedBy = "idEspaco",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    List<EspacosPacotes> EspacosPacotes = new ArrayList<>();

    @OneToMany(
            mappedBy = "idEspaco",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    List<Contratacao> contratacoes = new ArrayList<>();



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        String valorString = "R$ " + valor.toString();
        return valorString;
    }

    public void setValor(String valor) {
        valor = valor.replace(" ","");
        valor = valor.replace(",",".");
        valor = valor.replace("R$","");
        this.valor = Double.parseDouble(valor);
    }
}
