package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Usuario;
import com.coworking.coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario) {
        return repository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario editar(Usuario usuario, Integer id) {
        usuario.setId(id);
        return repository.save(usuario);
    }

    public List<Usuario> todosUsuarios(){
        return (List<Usuario>) repository.findAll();
    }

    public Usuario usuarioEspecifico( Integer id ) {
        Optional<Usuario> usuario = repository.findById(id);
        return usuario.get();
    }

    public boolean excluirUsuario( Integer id ){
        repository.deleteById(id);
        return true;
    }


}
