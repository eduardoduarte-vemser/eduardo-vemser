package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.Pagamentos;
import com.coworking.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentosService {

    @Autowired
    PagamentosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos salvar( Pagamentos pagamentos ) {
        
        return repository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos editar(Pagamentos pagamentos, Integer id) {
        pagamentos.setId(id);
        return repository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id) {
        repository.deleteById(id);
    }

    public List<Pagamentos> buscarTodos() {
        return (List<Pagamentos>) repository.findAll();
    }

}
