package com.coworking.coworking.Repository;

import com.coworking.coworking.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Cliente findByNome( String nome );
    Cliente findByCpf ( String cpf );

}
