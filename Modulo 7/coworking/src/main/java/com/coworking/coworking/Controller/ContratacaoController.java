package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Contratacao;
import com.coworking.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Contratacao> buscarTodos(){
        return service.buscarTodos();
    }

    @GetMapping("/buscarId/{id}")
    @ResponseBody
    public Contratacao buscarId( Integer id ) {
        return service.buscarPorId(id);
    }

    @PostMapping("/novo")
    @ResponseBody
    public Contratacao salvar(@RequestBody Contratacao contratacao) {
        return service.salvar( contratacao );
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Contratacao editar(@RequestBody Contratacao contratacao, @PathVariable Integer id) {
        return service.editar(contratacao,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public boolean deletar(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }

}
