package com.coworking.coworking.Entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "CLIENTES_PACOTES")
@SequenceGenerator(allocationSize = 1,name = "CLIENTES_PACOTES_SEQ",sequenceName = "CLIENTES_PACOTES_SEQ")
public class ClientesPacotes {

    @Id
    @GeneratedValue(generator = "CLIENTES_PACOTES_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Cliente idCliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_PACOTE",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Pacote idPacote;

    @Column(nullable = false)
    private Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public Pacote getIdPacote() {
        return idPacote;
    }

    public void setIdPacote(Pacote idPacote) {
        this.idPacote = idPacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
