package com.coworking.coworking.Entity;

import com.coworking.coworking.Repository.TipoContatoRepository;
import com.coworking.coworking.Service.TipoContatoService;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "CLIENTES")
@SequenceGenerator(allocationSize = 1,name = "CLIENTES_SEQ",sequenceName = "CLIENTES_SEQ")
public class Cliente {


    @Id
    @GeneratedValue(generator = "CLIENTES_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(name= "CPF",nullable = false,unique = true)
    private String cpf;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy" )
    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    @OneToMany(
            mappedBy = "idCliente",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )

    List<Contato> contato = new ArrayList<>();

    @OneToMany(
            mappedBy = "idCliente",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    @JsonIgnore
    List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany(
            mappedBy = "idCliente",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    @JsonIgnore
    List<Contratacao> contratacoes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Contato> getContato() {
        return contato;
    }

    public void setContato(List<Contato> contato) {
        for (int i = 0 ; i < contato.size() ; i ++) {
            contato.get(i).setIdCliente(this);
        }
        this.contato = contato;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
