package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Usuario;
import com.coworking.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Usuario> todosUsuarios() {
        return service.todosUsuarios();
    }

    @PostMapping("/novo")
    @ResponseBody
    public Usuario novoUsuario(@RequestBody Usuario usuario){
        return service.salvar(usuario);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Usuario editarUsuario(@PathVariable Integer id, @RequestBody Usuario usuario) {
        return service.editar(usuario,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public void excluirUsuario(@PathVariable Integer id) {
        service.excluirUsuario(id);
    }

}
