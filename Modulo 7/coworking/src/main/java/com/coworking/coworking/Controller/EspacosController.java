package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.Espaco;
import com.coworking.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {


    @Autowired
    EspacoService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Espaco> buscarTodos() {
        return service.buscarTodos();
    }

    @PostMapping("/novo")
    @ResponseBody
    public Espaco salvar (@RequestBody Espaco espaco) {
        return service.salvar(espaco);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Espaco editar ( @RequestBody Espaco espaco, @PathVariable Integer id ) {
        return service.editar(espaco,id);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public boolean deletar (Integer id) {
        service.deletar(id);
        return true;
    }


}
