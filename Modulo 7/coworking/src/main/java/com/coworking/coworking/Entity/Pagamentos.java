package com.coworking.coworking.Entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "PAGAMENTOS")
@SequenceGenerator(allocationSize = 1,name = "PAGAMENTOS_SEQ",sequenceName = "PAGAMENTOS_SEQ")
public class Pagamentos {

    @Id
    @GeneratedValue(generator = "PAGAMENTOS_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTES_PACOTES", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ClientesPacotes clientesPacotes;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CONTRATACAO")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Contratacao contratacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_PAGAMENTO")
    private TipoPagamento tipoPagamento;

    public Pagamentos(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Pagamentos(Contratacao contratacao) {
        this.contratacao = contratacao;
        this.clientesPacotes = clientesPacotes;
    }

    public Pagamentos(Contratacao contratacao, ClientesPacotes clientesPacotes) {
        this.contratacao = contratacao;
        this.clientesPacotes = clientesPacotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
