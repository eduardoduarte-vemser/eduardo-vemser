package com.coworking.coworking.Service;

import com.coworking.coworking.Entity.SaldoClientes;
import com.coworking.coworking.Entity.SaldoClientesId;
import com.coworking.coworking.Repository.SaldoClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClientesService {

    @Autowired
    SaldoClientesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoClientes salvar(SaldoClientes saldoClientes ) {
        return repository.save(saldoClientes);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoClientes editar(SaldoClientes saldoClientes, SaldoClientesId id) {
        saldoClientes.setId(id);
        return repository.save(saldoClientes);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(SaldoClientesId id) {
        repository.deleteById(id);
    }

    public List<SaldoClientes> buscarTodos() {
        return (List<SaldoClientes>) repository.findAll();
    }

}
