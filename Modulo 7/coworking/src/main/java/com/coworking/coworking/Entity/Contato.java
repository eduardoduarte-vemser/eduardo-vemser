package com.coworking.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CONTATOS")
@SequenceGenerator(allocationSize = 1,name = "CONTATOS_SEQ",sequenceName = "CONTATOS_SEQ")
public class Contato {

    @Id
    @GeneratedValue(generator = "CONTATOS_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "ID_TIPO_CONTATO", nullable = false )
    private TipoContato tipoContato;

    @Column( nullable = false )
    private String valor;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    @JsonIgnore
    private Cliente idCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void setIdCliente(Cliente cliente) {
        this.idCliente = cliente;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }
}
