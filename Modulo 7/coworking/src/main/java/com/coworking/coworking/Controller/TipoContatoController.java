package com.coworking.coworking.Controller;

import com.coworking.coworking.Entity.TipoContato;
import com.coworking.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<TipoContato> todosTiposContatos() {
        return service.todosTiposContato();
    }

    @GetMapping("/buscarPorNome/{nome}")
    @ResponseBody
    public TipoContato buscarPorNome(@PathVariable String nome) {
        return service.buscarPorNome( nome );
    }

    @PostMapping("/novo")
    @ResponseBody
    public TipoContato novoTipoContato(@RequestBody TipoContato tipoContato) {
        return service.salvar(tipoContato);
    }

    @DeleteMapping("/deletar/{id}")
    @ResponseBody
    public void deletar( Integer id ) {
        service.deletar(id);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public  TipoContato editar(@PathVariable Integer id, @RequestBody TipoContato tipoContato ) {
        return service.editar(tipoContato,id);
    }

}
