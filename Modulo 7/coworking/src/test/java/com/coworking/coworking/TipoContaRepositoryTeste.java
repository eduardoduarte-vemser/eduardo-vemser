package com.coworking.coworking;

import com.coworking.coworking.Entity.TipoContato;
import com.coworking.coworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TipoContaRepositoryTeste {

    @Autowired
    TipoContatoRepository repository;

    @Test
    public void testSaveClienteRepository() {

    }
        @Test
        @Transactional( rollbackFor = Exception.class)
        public void testGetTipo() {
            TipoContato email = new TipoContato();
            TipoContato telefone = new TipoContato();
            email.setNome("Email");
            repository.save(email);
            telefone.setNome("Telefone");
            repository.save(telefone);

            assertEquals("Telefone", repository.findByNome("Telefone").getNome());
            assertEquals("Email", repository.findByNome("Email").getNome());
    }
}
