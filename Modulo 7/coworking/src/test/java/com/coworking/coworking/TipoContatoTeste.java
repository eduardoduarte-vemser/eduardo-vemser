package com.coworking.coworking;

import com.coworking.coworking.Entity.TipoContato;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TipoContatoTeste {

    @Test
    public void testGetNome() {
        TipoContato tipoContato = new TipoContato();
        tipoContato.setNome("Email");
        Assertions.assertEquals(tipoContato.getNome(), "Email");
    }
}
