package com.coworking.coworking;

import com.coworking.coworking.Entity.Cliente;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClienteTeste {
    @Test
    public void testGetNomeEqual() {
        Cliente cliente = new Cliente();
        cliente.setNome("Eduardo");
        Assertions.assertEquals( "Eduardo", cliente.getNome() );
    }
    @Test
    public void testGetCpf() {
        Cliente cliente = new Cliente();
        cliente.setCpf( "000.000.000-00" );
        Assertions.assertEquals( "000.000.000-00", cliente.getCpf() );
    }

}
