package com.coworking.coworking;

import com.coworking.coworking.Entity.Cliente;
import com.coworking.coworking.Entity.Contato;
import com.coworking.coworking.Entity.TipoContato;
import com.coworking.coworking.Repository.ClienteRepository;
import com.coworking.coworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class ClienteRepositoryTeste {

    @Autowired
    ClienteRepository repository;

    @Test
    public void testGetCliente() throws ParseException {
        Cliente cliente = new Cliente();
        cliente.setNome("Eduardo");
        cliente.setCpf("000.000.000-00");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date data = new Date();
        data = (Date)format.parse("12/04/1991");
        cliente.setDataNascimento(data);
        repository.save(cliente);
        assertEquals("Eduardo",repository.findByNome("Eduardo").getNome());
        assertEquals(data,repository.findByNome("Eduardo").getDataNascimento());
    }

    @Test
    public void getClienteComContato() throws ParseException{
        Cliente cliente = new Cliente();
        Contato contato = new Contato();
        TipoContato tipoContato = new TipoContato();
        tipoContato.setId(1);
        tipoContato.setNome("Email");
        contato.setValor("dududu@dudud");
        cliente.setNome("Eduarsdo");
        cliente.setCpf("000.003.000-00");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date data = new Date();
        data = (Date)format.parse("12/04/1991");
        cliente.setDataNascimento(data);
        repository.save(cliente);
        assertEquals("Eduarsdo",repository.findByNome("Eduarsdo").getNome());
    }
}
